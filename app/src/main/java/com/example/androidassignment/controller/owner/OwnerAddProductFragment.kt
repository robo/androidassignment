package com.example.androidassignment.controller.owner


import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.fragment.app.activityViewModels
import com.example.androidassignment.R
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.controller.common.ImageFragment
import com.example.androidassignment.model.Product
import com.example.androidassignment.viewmodel.SharedViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

@AndroidEntryPoint
class OwnerAddProductFragment : ImageFragment(R.layout.fragment_owner_add_product) {
    companion object {
        private val TAG = OwnerAddProductFragment::class.java.name
    }

    private lateinit var productNameEditText: EditText
    private lateinit var productCategorySpinner: Spinner
    private lateinit var productDescriptionEditText: EditText
    private lateinit var productQuantityEditText: EditText
    private lateinit var productPriceEditText: EditText
    private lateinit var buttonSubmit: Button

    private var selectedCategoryId: String? = null

    private val model: SharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageView = view.findViewById(R.id.add_product_image_view)
        imageView.setOnClickListener {
            context?.let { context ->
                selectImage(context)
            }
        }

        productNameEditText = view.findViewById(R.id.add_product_name_edit_text)
        productCategorySpinner = view.findViewById(R.id.add_product_category_dropdown_menu)
        productDescriptionEditText = view.findViewById(R.id.add_product_description_edit_text)
        productQuantityEditText = view.findViewById(R.id.add_product_quantity_edit_text)
        productPriceEditText = view.findViewById(R.id.add_product_price_edit_text)

        /* populate category spinner */
        model.getCategories().observe(viewLifecycleOwner) { categories ->
            productCategorySpinner.adapter = context?.let { context ->
                ArrayAdapter(
                    context,
                    android.R.layout.simple_spinner_item,
                    categories.map { it.name }
                )
            }

            productCategorySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long,
                ) {
                    selectedCategoryId = categories[position].id
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    /* select first element by default */
                    parent?.setSelection(0)
                }

            }
        }

        buttonSubmit = view.findViewById(R.id.add_product_button_submit)
        buttonSubmit.setOnClickListener {
            if(!isDataValid()) {
                addProduct()
            }
            else{
                Snackbar
                    .make(view, getString(R.string.all_data_is_required), Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun isDataValid(): Boolean{
        return (productNameEditText.text.toString().trim().isEmpty()
                || productDescriptionEditText.text.toString().trim().isEmpty()
                || productPriceEditText.text.toString().trim().isEmpty()
                || productQuantityEditText.text.toString().trim().isEmpty()
        )
    }

    private fun addProduct() {
        GlobalScope.launch {
            val product = Product(
                null,
                model.currentUserId,
                selectedCategoryId,
                productNameEditText.text.toString(),
                productPriceEditText.text.toString().toFloat(),
                productDescriptionEditText.text.toString(),
                FirestorePath.PRODUCTS + "/" + UUID.randomUUID(),
                productQuantityEditText.text.toString().toInt()
            )

            if (model.saveProduct(product)
                && product.pictureURL?.let { model.saveData(it, getImageBytes()) } == true
            ) {
                withContext(Dispatchers.Main) {
                    Snackbar.make(requireView(), "Product successfully added", Snackbar.LENGTH_SHORT)
                        .show()
                }
            } else {
                withContext(Dispatchers.Main) {
                    Snackbar.make(requireView(), getString(R.string.internal_error), Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }
}