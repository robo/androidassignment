package com.example.androidassignment.controller.customer

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.adapter.OrderItemAdapter
import com.example.androidassignment.adapter.ProductAdapter
import com.example.androidassignment.model.Order
import com.example.androidassignment.model.OrderItem
import com.example.androidassignment.model.Product
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CustomerShoppingCartFragment : Fragment(R.layout.fragment_customer_shopping_cart) {
    companion object{
        val TAG = CustomerShoppingCartFragment::class.java.name
    }

    private var adapter: OrderItemAdapter? = null
    private lateinit var orderIdTextView: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var orderDetails: EditText
    private lateinit var totalPriceTextView: TextView
    private lateinit var confirmOrderButton: Button
    private val firestoreDB = Firebase.firestore
    private val model: SharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        orderIdTextView = view.findViewById(R.id.customer_shopping_cart_order_id)
        recyclerView = view.findViewById(R.id.customer_shopping_cart_recycler_view)

        totalPriceTextView = view.findViewById(R.id.customer_shopping_cart_total_price)
        confirmOrderButton = view.findViewById(R.id.customer_shopping_cart_confirm_order_button)

        model
            .getOrdersByCustomerIdByStatus(model.currentUserId, "pending")
            .observe(viewLifecycleOwner) { orderList ->
                confirmOrderButton.isEnabled = false
                if(orderList.size == 1){
                //solo il primo elemento mi interessa - c'è sempre solo un pending order per customer
                orderIdTextView.text = orderList[0].id
                totalPriceTextView.text = orderList[0].totalPrice.toString()

                confirmOrderButton.isEnabled = true

                confirmOrderButton.setOnClickListener {
                    GlobalScope
                        .launch {
                            val details: String? = orderDetails.text.toString()
                            val creationDate: Timestamp? = Timestamp.now()
                            val confirmedOrder = Order(
                                orderList[0].id,
                                model.currentUserId,
                                orderList[0].ownerId,
                                null,
                                "confirmed",
                                creationDate,
                                null,
                                details,
                                orderList[0].totalPrice,
                                false
                            )

                            if(model.saveOrder(confirmedOrder)){
                                Snackbar
                                    .make(view, "You successfully confirmed your order!", Snackbar.LENGTH_SHORT)
                                    .show()
                                //navigate back to dashboard
                                findNavController()
                                    .navigate(R.id.action_customerShoppingCartFragment_to_dashboardCustomerFragment)
                            }
                            else{
                                Log.i(TAG, "failed confirming order")
                                Snackbar
                                    .make(view, "Failed confirming order!", Snackbar.LENGTH_SHORT)
                                    .show()
                            }
                        }
                }

                    orderList[0].id?.let { loadData(it) }
                    orderDetails = view.findViewById(R.id.customer_shopping_cart_order_details)
                }
            }
    }

    private fun loadData(orderId: String){
        Log.i(TAG, "loadData()")

            //build query
            val query: Query = firestoreDB
                .collection("orderItems")
                .whereEqualTo("orderId", orderId)

            //build configuration for recyclerView
            val config = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(2)
                .setPageSize(10)
                .build()

            //firestorePagingOptions
            val options: FirestorePagingOptions<OrderItem> =
                FirestorePagingOptions.Builder<OrderItem>()
                    .setLifecycleOwner(this)
                    .setQuery(query, config, OrderItem::class.java)
                    .build()

            adapter = OrderItemAdapter(options)

            //set recyclerView
            recyclerView.setHasFixedSize(true)
            recyclerView.layoutManager = LinearLayoutManager(activity)

            //set adapter to recyclerView
            recyclerView.adapter = adapter
    }
}