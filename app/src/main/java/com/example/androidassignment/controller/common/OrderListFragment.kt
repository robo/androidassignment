package com.example.androidassignment.controller.common

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.adapter.OrderAdapter
import com.example.androidassignment.model.Order
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class OrderListFragment : Fragment(R.layout.fragment_order_list) {
    //fields
    val TAG: String = this.javaClass.simpleName
    private var firestoreDB: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var adapter: OrderAdapter? = null
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private lateinit var recyclerView: RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.order_list_recycler_view)

        val swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.order_list_swipe_refresh)
        swipeRefresh?.setOnRefreshListener {
            loadData()
            swipeRefresh.isRefreshing = false
        }

        loadData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "onCreate")
        super.onCreate(savedInstanceState)
    }

    private fun loadData(){
        //get the user role to create the proper query
        var role: String = "owner"
 /*       auth.currentUser?.let { firestoreDB.collection("users").document(it.uid).get()
            .addOnSuccessListener { documentSnapshot ->

            }
            .addOnFailureListener {
                Log.i(TAG, "failed getting user role")
            }
        }*/
        //Log.i(TAG, "user: ${auth.currentUser!!.uid} role: ${documentSnapshot["role"].toString()}")
        //role = documentSnapshot["role"].toString()

        //build query
        var query: Query = when(role){
            "customer" -> {
                Log.i(TAG, "customerId: ${auth.currentUser?.uid}")
                firestoreDB.collection("orders")
                    .whereEqualTo("customerId", auth.currentUser?.uid)
            }
            "rider" -> {
                Log.i(TAG, "riderId: ${auth.currentUser?.uid}")
                firestoreDB.collection("orders")
                    .whereEqualTo("riderId", auth.currentUser?.uid)
            }
            else -> {//owner
                Log.i(TAG, "ownerId: ${auth.currentUser?.uid}")
                firestoreDB.collection("orders")
                    .whereEqualTo("ownerId", auth.currentUser?.uid)
            }
        }


        //build configuration for recyclerView
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPrefetchDistance(10)
            .setPageSize(20)
            .build()

        Log.i(TAG, "config created")

        //firestorePagingOptions
        var options: FirestorePagingOptions<Order> = FirestorePagingOptions.Builder<Order>()
            .setLifecycleOwner(this)
            .setQuery(query, config, Order::class.java)
            .build()

        Log.i(TAG, "options created")

        //adapter = OrderAdapter(options, this.activity as AppCompatActivity)

        //set recyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        //set adapter to recyclerView
        recyclerView.adapter = adapter
        Log.i(TAG, "recyclerAdapter set")
    }
}