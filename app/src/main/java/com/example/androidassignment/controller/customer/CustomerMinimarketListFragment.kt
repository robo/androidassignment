package com.example.androidassignment.controller.customer

import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.adapter.MinimarketAdapter
import com.example.androidassignment.model.User
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class CustomerMinimarketListFragment() : Fragment(R.layout.fragment_customer_minimarket_list) {

    private val TAG: String = this.javaClass.simpleName
    private var adapter: MinimarketAdapter? = null
    private var firestoreDB: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var recyclerView: RecyclerView
    private val model: SharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.minimarket_list_recycler_view)

        var swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.customer_minimarket_list_swipe_refresh)
        swipeRefresh?.setOnRefreshListener {
            loadData()
            swipeRefresh.isRefreshing = false
        }

        loadData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "onCreate")
        super.onCreate(savedInstanceState)
    }

    private fun loadData() {
        //build query: get all owners
        var query: Query = firestoreDB.collection("users").whereEqualTo("role", "owner")
        Log.i(TAG, "${query.get().toString()}")
        Log.i(TAG, "query set")

        //build configuration for recyclerView
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPrefetchDistance(10)
            .setPageSize(20)
            .build()

        Log.i(TAG, "config created")

        //firestorePagingOptions on User model
        var options: FirestorePagingOptions<User> = FirestorePagingOptions.Builder<User>()
            .setLifecycleOwner(this)
            .setQuery(query, config, User::class.java)
            .build()

        Log.i(TAG, "options created")

        adapter = MinimarketAdapter(options, { ownerId ->
            /*select minimarket*/
            model.selectUser(ownerId)
            findNavController()
                .navigate(R.id.action_customerMinimarketListFragment_to_customerProductListFragment)
        },{ view, minimarket ->
            model.currentUser.observe(viewLifecycleOwner){ customer ->
                var distance = FloatArray(2)
                Location.distanceBetween(
                    customer.coordinates!!.latitude,
                    customer.coordinates!!.longitude,
                    minimarket.coordinates!!.latitude,
                    minimarket.coordinates!!.longitude,
                    distance
                )
                view.isVisible = (distance[0] < 10000)
            }
        })

        //set recyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity as AppCompatActivity)

        //set adapter to recyclerView
        recyclerView.adapter = adapter
        Log.i(TAG, "recyclerAdapter set")
    }
}