package com.example.androidassignment.controller.common

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.adapter.UserChatAdapter
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.model.Order
import com.example.androidassignment.model.User
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class UserListFragment : Fragment(R.layout.fragment_chat_user_list) {
    companion object{
        private val TAG = UserListFragment::class.java.name
    }
    private val firestore = Firebase.firestore
    private var adapter: UserChatAdapter? = null
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private val model: SharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.chat_user_list_recycler_view)
        swipeRefresh = view.findViewById(R.id.chat_user_list_swipe_refresh)

        swipeRefresh.setOnRefreshListener {
            loadData()
            swipeRefresh.isRefreshing = false
        }

        loadData()
    }

    private fun loadData(){
        model.currentUser.observe(viewLifecycleOwner) { currentUser ->
            when (currentUser.role){
                "owner" -> {
                    val query = firestore
                        .collection(FirestorePath.USERS)
                        .whereEqualTo("role", "rider")
                        .whereEqualTo("ownerId", currentUser.id)

                    // build configuration for recyclerView
                    val config = PagedList.Config.Builder()
                        .setEnablePlaceholders(false)
                        .setPrefetchDistance(2)
                        .setPageSize(10)
                        .build()

                    // firestorePagingOptions
                    val options: FirestorePagingOptions<User> = FirestorePagingOptions.Builder<User>()
                        .setLifecycleOwner(this)
                        .setQuery(query, config, User::class.java)
                        .build()

                    adapter = UserChatAdapter(options) { user ->
                        /* actions executed by onClickListener */
                        user.id?.let { id ->
                            model.selectChatRecipient(id)
                        }

                        findNavController()
                            .navigate(R.id.action_userListFragment_to_chatFragment)
                    }

                    // set recyclerView
//                    recyclerView.setHasFixedSize(true)
                    recyclerView.layoutManager = LinearLayoutManager(context)

                    //set adapter to recyclerView
                    recyclerView.adapter = adapter
                }
                "rider" -> {
                    // get the assigned orders of this rider and then get the user corresponding to
                    // the orders' customerId
                    firestore
                        .collection(FirestorePath.ORDERS)
                        .whereEqualTo("statusCode", "assigned")
                        .whereEqualTo("riderId", currentUser.id)
                        .get()
                        .addOnSuccessListener { ordersSnapshot ->
                            val orders = ordersSnapshot.toObjects(Order::class.java)
                            val customerIds = orders.map { it.customerId }.distinct()

                            val query = if (customerIds.isNotEmpty() ) {
                                 firestore
                                    .collection(FirestorePath.USERS)
                                    .whereIn(FieldPath.documentId(), customerIds) /* TODO: WRONG */
                            } else null

                            // build configuration for recyclerView
                            val config = PagedList.Config.Builder()
                                .setEnablePlaceholders(false)
                                .setPrefetchDistance(2)
                                .setPageSize(10)
                                .build()

                            // firestorePagingOptions
                            if (query != null) {
                                val options: FirestorePagingOptions<User> =
                                    FirestorePagingOptions.Builder<User>()
                                        .setLifecycleOwner(this)
                                        .setQuery(query, config, User::class.java)
                                        .build()

                                adapter = UserChatAdapter(options) { user ->
                                    /* actions executed by onClickListener */
                                    user.id?.let { id ->
                                        model.selectChatRecipient(id)
                                    }

                                    findNavController()
                                        .navigate(R.id.action_userListFragment_to_chatFragment)
                                }

                                //set recyclerView
                                activity?.runOnUiThread {
                                    recyclerView.setHasFixedSize(true)
                                    recyclerView.layoutManager = LinearLayoutManager(activity)

                                    //set adapter to recyclerView
                                    recyclerView.adapter = adapter
                                }
                            }
                        }
                        .addOnFailureListener {
                            view?.let { view ->
                                Snackbar
                                    .make(view, "Failed fetching customers", Snackbar.LENGTH_SHORT)
                                    .show()
                            }
                        }

                }
                "customer" -> {
                    /* se sono un customer, devo poter chattare con i riders che stanno consegnando i
                     * miei ordini, quindi devo ottenere i riderId degli ordini "assigned" di cui
                     * il customerId è il mio
                     */

                    // ottengo gli ordini "assigned" che ho fatto io (col mio customerId)
                    firestore
                        .collection(FirestorePath.ORDERS)
                        .whereEqualTo("statusCode", "assigned")
                        .whereEqualTo("customerId", currentUser.id)
                        .get()
                        .addOnSuccessListener { ordersSnapshot ->
                            val orders = ordersSnapshot.toObjects(Order::class.java)
                            val riderIds = orders.map { it.riderId }.distinct()

                            // ora ho una lista di userId -> mi serve una lista di users
                            val query = if (riderIds.isNotEmpty()) {
                                Log.i(TAG, "found ${riderIds.size} orders")
                                firestore
                                    .collection(FirestorePath.USERS)
                                    .whereIn(FieldPath.documentId(), riderIds)   /* TODO: WRONG */
                            } else {
                                Log.i(TAG, "no riders found")
                                null
                            }

                            //build configuration for recyclerView
                            val config = PagedList.Config.Builder()
                                .setEnablePlaceholders(false)
                                .setPrefetchDistance(2)
                                .setPageSize(10)
                                .build()

                            //firestorePagingOptions
                            if (query != null) {
                                Log.i(TAG, "query for riders is not null")
                                val options: FirestorePagingOptions<User> =
                                    FirestorePagingOptions.Builder<User>()
                                        .setLifecycleOwner(this)
                                        .setQuery(query, config, User::class.java)
                                        .build()

                                adapter = UserChatAdapter(options) { user ->
                                    /* actions executed by onClickListener */
                                    user.id?.let { id ->
                                        model.selectChatRecipient(id)
                                    }

                                    findNavController()
                                        .navigate(R.id.action_userListFragment_to_chatFragment)
                                }

                                //set recyclerView
                                //recyclerView.setHasFixedSize(true)
                                recyclerView.layoutManager = LinearLayoutManager(activity)

                                //set adapter to recyclerView
                                recyclerView.adapter = adapter
                            }
                        }
                        .addOnFailureListener { e ->
                            view?.let { view ->
                                Snackbar
                                    .make(view, "Failed fetching customers: ${e.message}", Snackbar.LENGTH_SHORT)
                                    .show()
                            }
                        }
                }
            }
        }
    }
}