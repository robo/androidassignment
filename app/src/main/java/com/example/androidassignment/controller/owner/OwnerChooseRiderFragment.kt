package com.example.androidassignment.controller.owner

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.adapter.RiderAdapter
import com.example.androidassignment.model.User
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class OwnerChooseRiderFragment : Fragment(R.layout.fragment_owner_rider_list) {
    companion object {
        private val TAG = OwnerChooseRiderFragment::class.java.name
    }

    private var adapter: RiderAdapter? = null
    private var firestoreDB: FirebaseFirestore = FirebaseFirestore.getInstance()
//    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private lateinit var recyclerView: RecyclerView
    private val model: SharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.rider_list_recycler_view)

        val swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.rider_list_swipe_refresh)
        swipeRefresh.setOnRefreshListener {
            loadData()
            swipeRefresh.isRefreshing = false
        }

        loadData()
    }

    private fun loadData(){
        val query: Query = firestoreDB
            .collection("users")
            .whereEqualTo("ownerId", model.currentUserId)
            .whereEqualTo("available", true)

        Log.i(TAG, "query set")

        //build configuration for recyclerView
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPrefetchDistance(10)
            .setPageSize(20)
            .build()

        Log.i(TAG, "config created")

        //firestorePagingOptions on User model
        val options: FirestorePagingOptions<User> = FirestorePagingOptions.Builder<User>()
            .setLifecycleOwner(this)
            .setQuery(query, config, User::class.java)
            .build()

        Log.i(TAG, "options created")

        adapter = RiderAdapter(options){ rider ->
            model.selectUser(rider)
            model.selectedOrder.observe(viewLifecycleOwner){ order ->
                order.riderId = rider.id
                order.statusCode = "waiting"

                Log.i(TAG, order.toString())

                GlobalScope.launch {
                    if (model.saveOrder(order)) {
                        Snackbar.make(requireView(), "Wait for ${model.selectedUser.value?.firstName} ${model.selectedUser.value?.lastName}'s acceptance", Snackbar.LENGTH_SHORT).show()

                        findNavController()
                            .navigate(R.id.action_ownerChooseRiderFragment_to_ownerOrdersTabFragment)
                    } else{
                        Snackbar.make(requireView(), getString(R.string.internal_error), Snackbar.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }

        //set recyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity as AppCompatActivity)

        //set adapter to recyclerView
        recyclerView.adapter = adapter
    }
}