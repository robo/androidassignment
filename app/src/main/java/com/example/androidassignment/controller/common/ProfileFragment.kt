package com.example.androidassignment.controller.common

import android.graphics.BitmapFactory
import androidx.fragment.app.activityViewModels
import com.example.androidassignment.model.User
import com.example.androidassignment.viewmodel.SharedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class ProfileFragment(fragmentView: Int): ImageFragment(fragmentView) {
    protected val model: SharedViewModel by activityViewModels()

    protected fun loadProfileImage() {
        GlobalScope.launch {
            val data = model.currentUser.value?.pictureURL?.let { model.getData(it) }

            if (data != null && data.isNotEmpty()) {
                withContext(Dispatchers.Main) {
                    imageView.setImageBitmap(
                        BitmapFactory.decodeByteArray(
                            data,
                            0,
                            data.size
                        )
                    )
                }
            }
        }
    }

    protected abstract fun updateProfile(user: User)
}