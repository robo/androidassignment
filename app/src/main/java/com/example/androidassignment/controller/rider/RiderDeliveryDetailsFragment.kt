package com.example.androidassignment.controller.rider

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassignment.R
import com.example.androidassignment.adapter.OrderAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class RiderDeliveryDetailsFragment : Fragment(R.layout.fragment_rider_delivery_details) {
    companion object {
        private val TAG = RiderDeliveryDetailsFragment::class.java.name
    }

    //fields
    private var firestoreDB: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var adapter: OrderAdapter? = null
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private lateinit var recyclerView: RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*TODO: qualcosa di analogo alla loadData()*/

    }

}