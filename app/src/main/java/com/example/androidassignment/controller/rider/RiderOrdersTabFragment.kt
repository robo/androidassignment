package com.example.androidassignment.controller.rider

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.androidassignment.R
import com.example.androidassignment.adapter.OrderTabsPagerAdapter
import com.example.androidassignment.controller.common.OrderTabFragment
import com.google.android.material.tabs.TabLayout

class RiderOrdersTabFragment: Fragment(R.layout.fragment_orders_tab) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpTabs()
    }

    private fun setUpTabs(){
        val adapter = OrderTabsPagerAdapter(childFragmentManager)

        adapter.addFragment(OrderTabFragment("waiting"), "Waiting")
        adapter.addFragment(OrderTabFragment("assigned"), "Assigned")
        adapter.addFragment(OrderTabFragment("delivered"), "Delivered")

        val viewPager = view?.findViewById<ViewPager>(R.id.owner_order_tabs_viewpager)
        viewPager?.adapter = adapter

        val tabs = view?.findViewById<TabLayout>(R.id.order_tabs_tablayout)
        tabs?.setupWithViewPager(viewPager)

        //set text for each tab
        for(i in 0..adapter.count){
            tabs?.getTabAt(i)?.text = adapter.getFragmentTitle(i)
        }
    }
}