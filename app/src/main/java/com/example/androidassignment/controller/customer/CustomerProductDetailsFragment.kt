package com.example.androidassignment.controller.customer

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.androidassignment.R
import com.example.androidassignment.model.Order
import com.example.androidassignment.model.OrderItem
import com.example.androidassignment.viewmodel.SharedViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import io.github.rosariopfernandes.firecoil.load
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CustomerProductDetailsFragment : Fragment(R.layout.fragment_customer_product_details) {
    companion object{
        val TAG: String = CustomerProductDetailsFragment::class.java.name
    }
    private lateinit var productName: TextView
    private lateinit var productImage: ImageView
    private lateinit var productDescription: TextView
    private lateinit var productPrice: TextView
    private lateinit var productQuantityEditText: EditText
    private var maxAvailableQuantity: Int? = null
    private lateinit var buttonPlus: ImageView
    private lateinit var buttonMinus: ImageView
    private lateinit var buttonAddToCart: Button
    private val firestoreDB = Firebase.firestore
    private val model: SharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        productName = view.findViewById(R.id.customer_product_details_product_name)
        productImage = view.findViewById(R.id.customer_product_details_product_image)
        productDescription = view.findViewById(R.id.customer_product_details_product_description)
        productPrice = view.findViewById(R.id.customer_product_details_price)
        productQuantityEditText = view.findViewById(R.id.customer_product_details_quantity_edit_text)
        productQuantityEditText.setText("0")
        buttonPlus = view.findViewById(R.id.customer_product_details_increase_quantity)
        buttonMinus = view.findViewById(R.id.customer_product_details_decrease_quantity)
        buttonAddToCart = view.findViewById(R.id.customer_product_details_button_add_to_cart)



        model.selectedProduct.observe(viewLifecycleOwner){ product ->
            productName.text = product.name

            product.pictureURL?.let { url ->
                model.getStorageReference(url)
            }?.let { ref ->
                view.findViewById<ImageView>(R.id.customer_product_details_product_image).load(ref){
                    crossfade(true)
                    placeholder(R.drawable.spinner)
                    error(R.drawable.not_found)
                }
            }

            productDescription.text = product.description
            productPrice.text = product.price.toString()

            maxAvailableQuantity = product.availableQuantity

            buttonMinus.setOnClickListener {
                // basically decreases value in editText by 1...
                if (Integer.parseInt(productQuantityEditText.text.toString()) > 0) {
                    productQuantityEditText.text = Editable.Factory.getInstance().newEditable((Integer.parseInt(productQuantityEditText.text.toString()) - 1).toString())
                }
                if(Integer.parseInt(productQuantityEditText.text.toString()) == 0){
                    buttonAddToCart.isEnabled = false
                }
            }

            buttonPlus.setOnClickListener {
                // basically increases value in editText by 1...
                if(Integer.parseInt(productQuantityEditText.text.toString()) == 0){
                    buttonAddToCart.isEnabled = true
                }
                if(Integer.parseInt(productQuantityEditText.text.toString()) < maxAvailableQuantity!!) {
                    productQuantityEditText.setText((Integer.parseInt(productQuantityEditText.text.toString()) + 1).toString())
                }
            }

            productQuantityEditText.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    if(productQuantityEditText.text.toString() == "") {
                        productQuantityEditText.setText("0")
                    }
                    if(productQuantityEditText.text.toString().toInt() > maxAvailableQuantity!!) {
                        productQuantityEditText.setText(maxAvailableQuantity!!.toString())
                    }
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if(productQuantityEditText.text.toString() == "") {
                        productQuantityEditText.setText("0")
                    }
                    if(productQuantityEditText.text.toString().toInt() > maxAvailableQuantity!!) {
                        productQuantityEditText.setText(maxAvailableQuantity!!.toString())
                    }
                }
            })
            buttonAddToCart.isEnabled = false
            buttonAddToCart.setOnClickListener {

                Log.i(TAG, "buttonAddToCart clicked")

                model.selectedProduct.observe(viewLifecycleOwner) { product ->
                    firestoreDB
                        .collection("orders")
                        .whereEqualTo("customerId", model.currentUserId)
                        .whereEqualTo("statusCode", "pending")
                        .get()
                        .addOnSuccessListener { orderSnapShot ->
                            //if 0 pending orders with customerId == currentUser.id
                            if (orderSnapShot.documents.size == 0) {

                                //se non ci sono pending orders per questo utente, creane uno
                                GlobalScope.launch{
                                    //create Order with:
                                    Log.i(TAG, "creating order")
                                    //random ID
                                    //order.ownerId = selectedProduct.ownerId
                                    //order.customerId = currentUser.id

                                    val order = Order(
                                        null,
                                        model.currentUserId,
                                        product.ownerId,
                                        null,
                                        "pending",
                                        null,
                                        null,
                                        null,
                                        0f,
                                        false
                                    )

                                    if (model.saveOrder(order)) {
                                        //create OrderItem with:
                                            //random ID
                                            //orderItem.orderId = order.id
                                            //orderItem.productId = product.id
                                            //orderItem.quantity = from EditText
                                            //orderItem.subTotal = product.price * quantity
                                        Log.i(TAG, "creating orderItem")
                                        val quantity: Int = productQuantityEditText.text.toString().toInt()
                                        val subTotal = quantity.times(product.price!!)
                                        val orderItem = OrderItem(
                                            null,
                                            product.id,
                                            order.id,
                                            quantity,
                                            subTotal
                                        )

                                        if (model.saveOrderItem(orderItem)) {
                                            Log.i(TAG, "orderItem saved")
                                            Snackbar
                                                .make(view, "Article(s) added to your cart!", Snackbar.LENGTH_SHORT)
                                                .show()

                                            findNavController()
                                                .navigate(R.id.action_customerProductDetailsFragment_to_customerProductListFragment)
                                        }
                                        else {
                                            Log.i(TAG, "Failed creating order!")
                                            Snackbar
                                                .make(view, "Failed creating order!", Snackbar.LENGTH_SHORT)
                                                .show()
                                        }
                                    }
                                }
                            }

                            //se esiste già un pending order per questo utente, cercare tra i suoi
                            //orderItems se ce n'è già uno corrispondente al prodotto da aggiungere,
                            //altrimenti lo si crea
                            else {
                                //if there already is ONE pending order, check if the ownerId is the same
                                if (orderSnapShot.documents[0]["ownerId"] == product.ownerId) {


                                    //l'ordine esiste già, controllo i suoi orderItems
                                    Log.i(TAG, "order already exists -> there must be at least one orderItem")
                                    firestoreDB
                                        .collection("orderItems")
                                        .whereEqualTo("orderId", orderSnapShot.documents[0].id)
                                        .whereEqualTo("productId", product.id)
                                        .get()
                                        .addOnSuccessListener { orderIdSnapshot ->

                                            //se c'è già un orderItem con lo stesso productId del
                                            //prodotto corrente, lo aggiorno
                                            if (orderIdSnapshot.documents.size >= 1) {
                                                //prendo il primo elemento
                                                orderIdSnapshot.documents[0].id
                                                //update quantity and subtotal
                                                GlobalScope.launch {
                                                    val newQuantity = productQuantityEditText.text.toString().toInt().plus(orderIdSnapshot.documents[0].getLong("quantity")!!.toInt())
                                                    val newSubtotal = product.price?.times(newQuantity)

                                                    val orderItem = OrderItem(
                                                        orderIdSnapshot.documents[0].id,
                                                        product.id,
                                                        orderSnapShot.documents[0].id,
                                                        newQuantity,
                                                        newSubtotal
                                                    )

                                                    if (model.saveOrderItem(orderItem)) {
                                                        Log.i(TAG, "updated existing orderItem")
                                                        Snackbar
                                                            .make(view, "Article(s) added to your cart!", Snackbar.LENGTH_SHORT)
                                                            .show()

                                                        findNavController()
                                                            .navigate(R.id.action_customerProductDetailsFragment_to_customerProductListFragment)
                                                    }
                                                    else {
                                                        Log.i(TAG, "failed updating orderItem to be added to order")
                                                        Snackbar
                                                            .make(view, "Failed updating orderItem to be added to order!", Snackbar.LENGTH_SHORT)
                                                            .show()
                                                    }
                                                }
                                            }

                                            //se non ci sono orderItems con lo stesso productId del
                                            //prodotto corrente, ne creo uno e lo aggancio all'ordine
                                            else {
                                                //create new orderItem
                                                GlobalScope.launch {
                                                    val quantity: Int = productQuantityEditText.text.toString().toInt()
                                                    val subTotal = quantity.times(product.price!!)
                                                    val orderItem = OrderItem(
                                                        null,
                                                        product.id,
                                                        orderSnapShot.documents[0].id,
                                                        quantity,
                                                        subTotal
                                                    )

                                                    if (model.saveOrderItem(orderItem)) {
                                                        Log.i(TAG, "created orderItem to be added to order")
                                                        Snackbar
                                                            .make(view, "Article(s) added to your cart!", Snackbar.LENGTH_SHORT)
                                                            .show()

                                                        findNavController()
                                                            .navigate(R.id.action_customerProductDetailsFragment_to_customerProductListFragment)
                                                    }
                                                    else {
                                                        Log.i(TAG, "failed creating orderItem")
                                                        Snackbar
                                                            .make(view, "Failed creating orderItem", Snackbar.LENGTH_SHORT)
                                                            .show()
                                                    }
                                                }
                                            }
                                        }
                                        .addOnFailureListener {
                                            Log.i(TAG, "Failed fetching orderItems")
                                            Snackbar
                                                .make(view, "Failed fetching orderItems", Snackbar.LENGTH_SHORT)
                                                .show()
                                        }
                                }
                                else {
                                    Log.i(TAG, "There already is a pending order! Cannot proceed!")
                                    Snackbar
                                        .make(view, "You can only order from one shop at a time!", Snackbar.LENGTH_SHORT)
                                        .show()
                                }
                            }
                        }
                        .addOnFailureListener {
                            Log.i(TAG, "Failed fetching orders")
                            Snackbar
                                .make(view, "Failed fetching orders", Snackbar.LENGTH_SHORT)
                                .show()
                        }
                }
            }
        }
    }
}