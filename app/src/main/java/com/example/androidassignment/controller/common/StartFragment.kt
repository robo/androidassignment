package com.example.androidassignment.controller.common

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.androidassignment.R
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.auth.AuthUI
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StartFragment : Fragment(R.layout.fragment_start) {
    companion object {
        private val TAG = StartFragment::javaClass.name
        private const val RC_SIGN_IN = 1234
    }

    val model: SharedViewModel by activityViewModels()
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.currentUser.observe(viewLifecycleOwner) { currentUser ->
            if (currentUser.id != null) {
                currentUser.role?.let { goToDashboard(it) }
            }
        }

        view.findViewById<Button>(R.id.button_signin)
            .setOnClickListener {
                /* starting drop-in Firestore UI Auth login flow */
                val providers = arrayListOf(
                    AuthUI.IdpConfig.EmailBuilder()
                        .setAllowNewAccounts(false)
                        .build(),
                )

                startActivityForResult(
                    AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                    RC_SIGN_IN)
            }

        view.findViewById<Button>(R.id.button_signup)
            .setOnClickListener {
                findNavController().navigate(R.id.action_startFragment_to_chooseUserTypeFragment)
            }

    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        val response = IdpResponse.fromResultIntent(data)
//
//        if (requestCode == RC_SIGN_IN) {
//            /* successfully signed in */
//            if (resultCode == AppCompatActivity.RESULT_OK) {
//
//            }
//            else {
//                /* sign in failed */
//                if (response == null) {
//                    /* nothing */
//                } else {
//                    /* nothing */
//                }
//            }
//        }
//    }

    private fun goToDashboard(role: String) {
        var action: NavDirections? = null

        when (role) {
            "owner" -> {
                action =
                    StartFragmentDirections.actionStartFragmentToDashboardOwnerFragment()
            }
            "rider" -> {
                action =
                    StartFragmentDirections.actionStartFragmentToDashboardRiderFragment()
            }
            "customer" -> {
                action =
                    StartFragmentDirections.actionStartFragmentToDashboardCustomerFragment()
            }
        }

        if (action != null) {
            model.currentUser.removeObservers(this)

            Log.i(TAG, "navigating from start fragment to dashboard")

            findNavController().navigate(action)
        }
    }
}