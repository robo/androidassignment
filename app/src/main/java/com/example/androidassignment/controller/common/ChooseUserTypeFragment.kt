package com.example.androidassignment.controller.common

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.androidassignment.R
import com.example.androidassignment.viewmodel.SharedViewModel

class ChooseUserTypeFragment : Fragment(R.layout.fragment_choose_user_type) {
    val model: SharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.choose_user_type_owner_button)
            .setOnClickListener {
                findNavController().navigate(R.id.action_chooseUserTypeFragment_to_ownerSignUpFragment)
            }

        view.findViewById<Button>(R.id.choose_user_type_rider_button)
            .setOnClickListener {
                findNavController().navigate(R.id.action_chooseUserTypeFragment_to_riderSignUpFragment)
            }

        view.findViewById<Button>(R.id.choose_user_type_customer_button)
            .setOnClickListener {
                findNavController().navigate(R.id.action_chooseUserTypeFragment_to_customerSignUpFragment)
            }
    }
}