package com.example.androidassignment.controller.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.androidassignment.R
import com.example.androidassignment.viewmodel.SharedViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CustomerDashboardFragment : Fragment() {
    companion object {
        //private val TAG = DashboardCustomerFragment::javaClass.name
    }

    private val model: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val layout = inflater.inflate(R.layout.fragment_customer_dashboard, container, false)

        layout.findViewById<CardView>(R.id.dashboard_customer_grid_profile)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardCustomerFragment_to_customerProfileFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_customer_grid_orders)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardCustomerFragment_to_customerOrdersTabFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_customer_grid_minimarkets)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardCustomerFragment_to_minimarketListFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_customer_grid_shopping_cart)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardCustomerFragment_to_customerShoppingCartFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_customer_grid_riders_chat)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardCustomerFragment_to_userListFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_customer_grid_logout)
            .setOnClickListener {
                model.signOut()

                model.currentUser.observe(viewLifecycleOwner) { currentUser ->
                    if (currentUser.id == null) {
                        val action = (R.id.action_global_startFragment)

                        findNavController()
                            .navigate(action)
                    }
                }
            }

        return layout
    }
}