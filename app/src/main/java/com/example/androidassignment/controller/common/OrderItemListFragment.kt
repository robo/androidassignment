package com.example.androidassignment.controller.common

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.adapter.OrderItemAdapter
import com.example.androidassignment.model.OrderItem
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class OrderItemListFragment(private var orderId: String?) : Fragment(R.layout.fragment_order_item_list) {
    //fields
    val TAG: String = this.javaClass.simpleName
    private var firestoreDB: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var adapter: OrderItemAdapter? = null
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private lateinit var recyclerView: RecyclerView
    private var viewThis: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "onCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.orderitem_list_recycler_view)
        Log.i(TAG, "culo ${recyclerView.toString()}")

        var swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.orderitem_list_swipe_refresh)
        swipeRefresh?.setOnRefreshListener {
            //forse è la cosa più sensata da fare
            orderId?.let { loadData(it) }
            swipeRefresh.isRefreshing = false
        }

        if(auth.currentUser == null) {
            auth.signInWithEmailAndPassword("test@mail.com", "testPassword")
                .addOnCompleteListener(activity as AppCompatActivity) { task ->
                    if (task.isSuccessful) {
                        Log.i(TAG, "signIn:success")
                        //then load data into recyclerView

                        orderId?.let { loadData(it) }
                    } else {
                        Log.i(TAG, "signIn:failure")
                    }
                }
        }
        else{
            orderId?.let { loadData(it) }
        }
    }

    fun loadData(orderId: String){
        auth.currentUser?.uid?.let { Log.i(TAG, it) }

//        recyclerView = requireView().findViewById(R.id.orderitem_list_recycler_view)
        //build query with orderId passed by parameter
        var query: Query = firestoreDB.collection("orderItems").whereEqualTo("orderId", orderId)

        //firestoreDB.collection("orderItems").whereEqualTo("orderId", orderId).get()
            query
            .get()
            .addOnSuccessListener { querySnapshot ->
                Log.i(TAG, "found ${querySnapshot.documents.size} items")
            }
            .addOnFailureListener {
                Log.i(TAG, "failed fetching orderitems for order $orderId")
            }



        //build configuration for recyclerView
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPrefetchDistance(10)
            .setPageSize(20)
            .build()

        Log.i(TAG, "config created")

        //firestorePagingOptions
        var options: FirestorePagingOptions<OrderItem> = FirestorePagingOptions.Builder<OrderItem>()
            .setLifecycleOwner(this)
            .setQuery(query, config, OrderItem::class.java)
            .build()

        Log.i(TAG, "options created")

        adapter = OrderItemAdapter(options)

        //set recyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity as AppCompatActivity)

        if(adapter == null){
            Log.i(TAG, "adapter is null!")
        }

        //set adapter to recyclerView
        recyclerView.adapter = adapter
        Log.i(TAG, "recyclerAdapter set")
    }
}