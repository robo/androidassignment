package com.example.androidassignment.controller.owner

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.androidassignment.R
import com.example.androidassignment.controller.common.ImageFragment
import com.example.androidassignment.model.Product
import com.example.androidassignment.viewmodel.SharedViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import io.github.rosariopfernandes.firecoil.load
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class OwnerProductDetailsFragment : ImageFragment(R.layout.fragment_owner_product_details) {
    companion object {
        private val TAG = OwnerProductDetailsFragment::class.java.name
    }

    private lateinit var productNameEditText: EditText
    private lateinit var productPriceEditText: EditText
    private lateinit var productDescriptionEditText: EditText
    private lateinit var buttonPlus: ImageView
    private lateinit var buttonMinus: ImageView
    private lateinit var productQuantityEditText: EditText
    private lateinit var buttonSubmit: Button

    private val model: SharedViewModel by activityViewModels()

    private lateinit var oldProductName: String
    private lateinit var oldProductDescription: String
    private lateinit var oldProductPrice: String
    private var oldQuantity: Int = 0
    private var imageHasChanged: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val product = model.selectedProduct.value

        oldProductName = product?.name.toString()
        oldProductDescription = product?.description.toString()
        oldProductPrice = product?.price.toString()


        buttonSubmit = view.findViewById(R.id.owner_product_details_submit)

        productNameEditText = view.findViewById(R.id.product_details_product_name)
        productNameEditText.addTextChangedListener(CustomWatcher(this, buttonSubmit))

        imageView = view.findViewById(R.id.product_details_product_image)
        imageView.setOnClickListener {
            context?.let {
                context -> selectImage(context)
            }
            imageHasChanged = true
        }

        productDescriptionEditText = view.findViewById(R.id.product_details_product_description)
        productDescriptionEditText.addTextChangedListener(CustomWatcher(this, buttonSubmit))

        productPriceEditText = view.findViewById(R.id.product_details_price_edit_text)
        productPriceEditText.addTextChangedListener(CustomWatcher(this, buttonSubmit))

        productQuantityEditText = view.findViewById(R.id.product_details_quantity_edit_text)
        productQuantityEditText.addTextChangedListener(CustomWatcher(this, buttonSubmit))

        buttonMinus = view.findViewById(R.id.product_details_decrease_quantity)
        buttonPlus = view.findViewById(R.id.product_details_increase_quantity)


        buttonSubmit.isEnabled = false
        buttonSubmit.setOnClickListener {
            //Log.d(TAG, "avail: " + productQuantityEditText.text)

            oldQuantity = Integer.parseInt(productQuantityEditText.text.toString())
            val newProductName = productNameEditText.text.toString()
            val newProductDescription = productDescriptionEditText.text.toString()
            val newProductPrice = productPriceEditText.text.toString().toFloat()
            val newProductAvailableQuantity = productQuantityEditText.text.toString().toInt()

            /*update product fields for update*/
            product?.name = newProductName
            product?.description = newProductDescription
            product?.price = newProductPrice
            product?.availableQuantity = newProductAvailableQuantity

            if (product != null) {
                GlobalScope.launch {
                    if (model.saveProduct(product)
                        && product.pictureURL?.let { pictureURL -> model.saveData(pictureURL, getImageBytes()) } == true
                    ) {
                        withContext(Dispatchers.Main) {
                            Snackbar.make(view, "Product successfully updated", Snackbar.LENGTH_SHORT)
                                .show()
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            Snackbar.make(view, "An internal error has occured", Snackbar.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            } else {
                Snackbar.make(view, "An internal error has occured", Snackbar.LENGTH_SHORT)
                    .show()
            }

            buttonSubmit.isEnabled = false
        }

        /*quantity EditText*/
        productQuantityEditText.addTextChangedListener(CustomWatcher(this, buttonSubmit))/*object:TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if(productQuantityEditText.text.toString() != "") {
                    buttonSubmit.isEnabled = oldQuantity != Integer.parseInt(productQuantityEditText.text.toString())
                }
                else{
                    productQuantityEditText.text = Editable.Factory.getInstance().newEditable("0")
                    buttonSubmit.isEnabled = false
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(productQuantityEditText.text.toString() != "") {
                    buttonSubmit.isEnabled = oldQuantity != Integer.parseInt(productQuantityEditText.text.toString())
                }
                else{
                    productQuantityEditText.text = Editable.Factory.getInstance().newEditable("0")
                    buttonSubmit.isEnabled = false
                }
            }
        })*/

        buttonMinus.setOnClickListener {
            // basically decreases value in editText by 1...
            if (Integer.parseInt(productQuantityEditText.text.toString()) > 0) {
                productQuantityEditText.text = Editable.Factory.getInstance().newEditable((Integer.parseInt(productQuantityEditText.text.toString()) - 1).toString())
            }

            buttonSubmit.isEnabled = oldQuantity != Integer.parseInt(productQuantityEditText.text.toString())
        }

        buttonPlus.setOnClickListener {
            // basically increases value in editText by 1...
            productQuantityEditText.setText((Integer.parseInt(productQuantityEditText.text.toString()) + 1).toString())
            buttonSubmit.isEnabled = oldQuantity != Integer.parseInt(productQuantityEditText.text.toString())
        }

        /* update view */
        model.selectedProduct.observe(viewLifecycleOwner) { product ->
            productNameEditText.text = Editable.Factory.getInstance().newEditable(product.name.toString())

            Log.i(TAG, "product name: " + product.name)

            product.pictureURL?.let { url ->
                model.getStorageReference(url)
            }?.let { ref ->
                view.findViewById<ImageView>(R.id.product_details_product_image).load(ref){
                    crossfade(true)
                    placeholder(R.drawable.spinner)
                    error(R.drawable.not_found)
                }
            }

            view.findViewById<TextView>(R.id.product_details_product_description).text = product.description
            view.findViewById<EditText>(R.id.product_details_price_edit_text).text = Editable.Factory.getInstance().newEditable((product.price).toString())

            oldQuantity = product.availableQuantity!! /* needed to disable button if same old quantity */
            productQuantityEditText.setText(product.availableQuantity.toString())
        }
    }

    private fun dataHasChanged(): Boolean{
        return (oldProductName != productNameEditText.text.toString()
                || oldProductDescription != productDescriptionEditText.text.toString()
                || oldProductPrice != productPriceEditText.text.toString()
                || oldQuantity != productQuantityEditText.text.toString().toInt()
                || imageHasChanged)
    }

    private fun isDataValid(): Boolean{
        return productNameEditText.text.toString() != ""
            && productPriceEditText.text.toString() != ""
            && productPriceEditText.text.toString().toFloat() > 0f
            && productQuantityEditText.text.toString() != ""
            && productQuantityEditText.text.toString().toInt() >= 0
    }

    /*submit button is only enabled when some data has changed and if all data is valid*/
    class CustomWatcher(private val fragment: OwnerProductDetailsFragment, private val button: Button): TextWatcher{
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            button.isEnabled = fragment.dataHasChanged() && fragment.isDataValid()
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            button.isEnabled = fragment.dataHasChanged() && fragment.isDataValid()
        }

        override fun afterTextChanged(p0: Editable?) {
            button.isEnabled = fragment.dataHasChanged() && fragment.isDataValid()
        }
    }
}