package com.example.androidassignment.controller.common

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.example.androidassignment.R
import java.io.ByteArrayOutputStream

/* A fragment with an uploadable image */
open class ImageFragment(fragmentViewId: Int): Fragment(fragmentViewId) {
    companion object {
        private val TAG = ImageFragment::class.java.name
    }

    var hasUploadedImage: Boolean = false

    protected lateinit var imageView: ImageView

    /* two methods to get image from user */
    private var profileBitmap: Bitmap? = null
    private var profileURI: Uri? = null

    fun selectImage(context: Context) {
        val options = arrayOf<CharSequence>(getString(R.string.take_picture), getString(R.string.upload_picture), getString(
            R.string.cancel))
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)

        builder
            .setTitle(getString(R.string.choose_picture))
            .setItems(options) { dialog, item ->
                when {
                    options[item] == getString(R.string.take_picture) -> {
                        val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(takePicture, 0)
                    }
                    options[item] == getString(R.string.upload_picture) -> {
                        val pickPicture =
                            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        startActivityForResult(pickPicture, 1)
                    }
                    options[item] == getString(R.string.cancel) -> {
                        dialog.dismiss()
                    }
                }
            }
            .show()
    }

    fun getImageBytes(): ByteArray {
        ByteArrayOutputStream().apply {
            when {
                profileBitmap != null -> {
                    Log.i(TAG, "converting image from bitmap to compressed byte stream")

                    profileBitmap?.compress(Bitmap.CompressFormat.JPEG, 10, this)
                }
                profileURI != null -> {
                    val iStream = activity?.contentResolver?.openInputStream(profileURI!!)

                    iStream?.copyTo(this)
                }
                else -> {
                    Log.i(TAG, "bitmap is null!")
                }
            }

            return toByteArray()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0) {             /* took picture with camera */
            val rawData = data?.extras?.get("data")

            if (rawData != null) {
                hasUploadedImage = true
                profileURI = null               /* clear URI */

                profileBitmap = rawData as Bitmap
                imageView.setImageBitmap(profileBitmap)
            }
        } else if (requestCode == 1) {      /* chose picture from gallery */
            hasUploadedImage = true

            profileBitmap = null            /* clear bitmap */
            profileURI = data?.data

            imageView.setImageURI(data?.data)
        }
    }
}