package com.example.androidassignment.controller.common

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.adapter.OrderAdapter
import com.example.androidassignment.controller.customer.CustomerOrdersTabFragmentDirections
import com.example.androidassignment.controller.owner.OwnerOrdersTabFragmentDirections
import com.example.androidassignment.controller.rider.RiderOrdersTabFragmentDirections
import com.example.androidassignment.model.Order
import com.example.androidassignment.model.User
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class OrderTabFragment(private var statusCode: String) : Fragment(R.layout.fragment_orders_generic) {
    companion object {
        private val TAG = OrderTabFragment::class.java.name
    }

    private var firestoreDB: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var adapter: OrderAdapter? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var trackAllRidersButton: Button
    private lateinit var seeAllDestinationsOnMapButton: Button

    private val model: SharedViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i(TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.order_generic_tabs_recycler_view)
        trackAllRidersButton = view.findViewById(R.id.order_generic_track_all_riders_button)
        trackAllRidersButton.isVisible = false
        seeAllDestinationsOnMapButton =
            view.findViewById(R.id.order_generic_see_all_destinations_on_map_button)
        seeAllDestinationsOnMapButton.isVisible = false

        model.currentUser.observe(viewLifecycleOwner) { currentUser ->
            if ((currentUser.role == "owner" || currentUser.role == "customer") && statusCode == "assigned") {
                trackAllRidersButton.isVisible = true

                /* on click, go to map fragment to track all riders */
                trackAllRidersButton.setOnClickListener {

                    if (currentUser.role == "owner") {
                        /* get riders by owner id that have at least one assigned order (in 2 steps) */
                        model.getOrdersByOwnerIdByStatus(model.currentUserId, statusCode)
                            .observe(viewLifecycleOwner) { orders ->
                                val riderIds = orders.map { it.riderId }.distinct()
                                val riders = mutableListOf<User>()

                                GlobalScope.launch {
                                    for (riderId in riderIds) {
                                        if (riderId != null) {
                                            model.getUserSync(riderId)?.let { rider -> riders.add(rider) }
                                        }
                                    }

                                    model.selectUsers(riders)

                                    withContext(Dispatchers.Main) {
                                        val action = OwnerOrdersTabFragmentDirections.actionOwnerOrdersTabFragmentToMapFragment()

                                        findNavController()
                                            .navigate(action)
                                    }
                                }
                            }
                    } else if (currentUser.role == "customer") {
                        /* get riders by customer id that have at leats one assigned order (in 2 steps) */
                        model.getOrdersByCustomerIdByStatus(model.currentUserId, statusCode)
                            .observe(viewLifecycleOwner) { orders ->
                                val riderIds = orders.map { it.riderId }.distinct()
                                val riders = mutableListOf<User>()

                                GlobalScope.launch {
                                    for (riderId in riderIds) {
                                        if (riderId != null) {
                                            model.getUserSync(riderId)?.let { rider -> riders.add(rider) }
                                        }
                                    }

                                    model.selectUsers(riders)

                                    withContext(Dispatchers.Main) {
                                        val action = CustomerOrdersTabFragmentDirections.actionCustomerOrdersTabFragmentToMapFragment()

                                        findNavController()
                                            .navigate(action)
                                    }
                                }
                            }
                    }
                }
            }


            if (currentUser.role == "rider" && statusCode == "assigned") {
                seeAllDestinationsOnMapButton.isVisible = true
                seeAllDestinationsOnMapButton.setOnClickListener {
                    /* on click, go to map fragment to track all customers */
                    model.getOrdersByRiderIdByStatus(model.currentUserId, statusCode)
                        .observe(viewLifecycleOwner) { orders ->
                            val customerIds = orders.map { it.customerId }.distinct()
                            val customers = mutableListOf<User>()

                            Log.i(TAG, "found ${customers.size} customers")

                            GlobalScope.launch {
                                for (customerId in customerIds) {
                                    if (customerId != null) {
                                        model.getUserSync(customerId)?.let { customer -> customers.add(customer) }
                                    }
                                }

                                model.selectUsers(customers)

                                withContext(Dispatchers.Main) {
                                    val action = RiderOrdersTabFragmentDirections.actionRiderOrdersTabFragmentToMapFragment()

                                    findNavController()
                                        .navigate(action)
                                }
                            }
                        }
                }
            }
        }

        val swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.order_generic_tab_swipe_refresh)
        swipeRefresh?.setOnRefreshListener {
            loadData()
            swipeRefresh.isRefreshing = false
        }

        loadData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "onCreate")
        super.onCreate(savedInstanceState)
    }

    private fun loadData(){
        //build query
        model.currentUser.observe(viewLifecycleOwner){ currentUser ->
            val query: Query? = when(currentUser.role){
                "owner" -> {
                    firestoreDB.collection("orders")
                        .whereEqualTo("ownerId", currentUser.id)
                        .whereEqualTo("statusCode", statusCode)
                }
                "rider" -> {
                    firestoreDB
                        .collection("orders")
                        .whereEqualTo("riderId", currentUser.id)
                        .whereEqualTo("statusCode", statusCode)
                }
                "customer" -> {
                    //show both confirmed and waiting or singularly the others
                    if(statusCode == "confirmed") {
                        val values = listOf("confirmed", "waiting")
                        firestoreDB.collection("orders")
                            .whereIn("statusCode", values)
                            .whereEqualTo("customerId", currentUser.id)
                    }
                    //show singular statusCode
                    else{
                        firestoreDB.collection("orders")
                            .whereEqualTo("customerId", currentUser.id)
                            .whereEqualTo("statusCode", statusCode)
                    }
                }
                else -> {
                    null
                }
            }

            if (query != null) {
                //build configuration for recyclerView
                val config = PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setPrefetchDistance(10)
                    .setPageSize(20)
                    .build()

                //firestorePagingOptions
                val options: FirestorePagingOptions<Order> = FirestorePagingOptions.Builder<Order>()
                    .setLifecycleOwner(this)
                    .setQuery(query, config, Order::class.java)
                    .build()

                adapter = OrderAdapter(options, this.activity as AppCompatActivity, model.currentUser,
                    { order ->
                        model.selectOrder(order)
                        findNavController()
                            .navigate(R.id.action_ownerOrdersTabFragment_to_ownerChooseRiderFragment)
                    },
                    { order ->
                        when (currentUser.role) {
                            "customer" -> {
                                GlobalScope.launch {
                                    if (order.riderId != null) {
                                        val rider = model.getUserSync(order.riderId!!)

                                        if (rider != null) {
                                            model.selectUsers(listOf(rider))

                                            withContext(Dispatchers.Main) {
                                                val action = CustomerOrdersTabFragmentDirections
                                                    .actionCustomerOrdersTabFragmentToMapFragment()

                                                findNavController()
                                                    .navigate(action)
                                            }
                                        }
                                    }
                                }

                            }
                            "owner" -> {
                                GlobalScope.launch {
                                    if (order.riderId != null) {
                                        val rider = model.getUserSync(order.riderId!!)

                                        if (rider != null) {
                                            model.selectUsers(listOf(rider))

                                            withContext(Dispatchers.Main) {
                                                val action = OwnerOrdersTabFragmentDirections
                                                    .actionOwnerOrdersTabFragmentToMapFragment()

                                                findNavController()
                                                    .navigate(action)
                                            }
                                        }
                                    }
                                }
                            }
                            "rider" -> {
                                GlobalScope.launch {
                                    if (order.customerId != null) {
                                        val customer = model.getUserSync(order.customerId!!)

                                        if (customer != null) {
                                            model.selectUsers(listOf(customer))

                                            withContext(Dispatchers.Main) {
                                                val action = RiderOrdersTabFragmentDirections
                                                    .actionRiderOrdersTabFragmentToMapFragment()

                                                findNavController()
                                                    .navigate(action)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    })

                //set recyclerView
                recyclerView.setHasFixedSize(true)
                recyclerView.layoutManager = LinearLayoutManager(activity)

                //set adapter to recyclerView
                recyclerView.adapter = adapter
            }
        }
    }
}