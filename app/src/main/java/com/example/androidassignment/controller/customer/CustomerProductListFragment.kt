package com.example.androidassignment.controller.customer

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.adapter.ProductAdapter
import com.example.androidassignment.model.Category
import com.example.androidassignment.model.Product
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlin.collections.ArrayList

class CustomerProductListFragment : Fragment(R.layout.fragment_product_list) {
    companion object{
        private val TAG = CustomerProductListFragment::class.java.name
    }

    private var firestoreDB = Firebase.firestore
    private var adapter: ProductAdapter? = null
    private var auth = Firebase.auth
    private lateinit var recyclerView: RecyclerView
    private lateinit var spinner: Spinner
    private val model: SharedViewModel by activityViewModels()
    private var categories: ArrayList<Category> = ArrayList()
    private var categoriesList: ArrayList<String> = ArrayList()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.product_list_recycler_view)
        spinner = view.findViewById(R.id.product_list_dropdown_menu)

        val swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.product_list_swipe_refresh)
        swipeRefresh?.setOnRefreshListener {
            categories[spinner.selectedItemPosition].id?.let { categoryId ->
                auth.currentUser?.uid?.let { ownerId ->
                    loadData(ownerId, categoryId)
                } }
            swipeRefresh.isRefreshing = false
        }

        populateSpinner()
    }

    private fun populateSpinner(){
        //get all categories ordered by index
        firestoreDB
            .collection("categories")
            .get()
            .addOnSuccessListener {
                //add default category
                categories.add(Category("default", "Tutte le categorie"))

                for (item in it.documents) {
                    categories.add(Category(item.id, item["name"] as String))
                    //item.toObject<Category>()?.let { it1 -> categories.add(it1) }
                }

                Log.i(TAG, "categories fetched")

                for(i in 0 until categories.size){
                    categories[i].name?.let { categoryName -> categoriesList.add(categoryName) }
                }

                spinner.adapter = activity?.let { activity ->
                    ArrayAdapter(activity, R.layout.support_simple_spinner_dropdown_item, categoriesList)
                }
                spinner.onItemSelectedListener = auth.currentUser?.uid?.let { ownerId ->
                    SpinnerOnItemSelectedListener(this,
                        ownerId, categories)
                }

                //call loadData()
                model.selectedUser.observe(viewLifecycleOwner){ owner ->
                    categories[0].id?.let { categoryId ->
                        owner.id?.let { ownerId ->
                            loadData(ownerId, categoryId)
                        }
                    }
                }
            }
            .addOnFailureListener {
                Log.i(TAG, "failed fetching categories")
            }
    }

    private fun loadData(ownerId: String, categoryId: String){
        Log.i(TAG, "loadData($categoryId)")
        //build query

        model.selectedUser.observe(viewLifecycleOwner) { owner ->
            val query = when (categoryId) {
                "default" -> {
                    //get all products when category is not specified
                    firestoreDB
                        .collection("products")
                        .whereEqualTo("ownerId", owner.id)
                        .whereNotEqualTo("availableQuantity", 0)
                }
                else -> {
                    firestoreDB
                        .collection("products")
                        .whereEqualTo("ownerId", owner.id)
                        .whereEqualTo("categoryId", categoryId)
                        .whereNotEqualTo("availableQuantity", 0)
                }
            }

            //build configuration for recyclerView
            val config = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(2)
                .setPageSize(10)
                .build()

            //firestorePagingOptions
            val options: FirestorePagingOptions<Product> = FirestorePagingOptions.Builder<Product>()
                .setLifecycleOwner(this)
                .setQuery(query, config, Product::class.java)
                .build()

            adapter = ProductAdapter(options) {
                /* actions executed by onClickListener */
                model.selectProduct(it)
                findNavController()
                    .navigate(CustomerProductListFragmentDirections.actionCustomerProductListFragmentToCustomerProductDetailsFragment())
            }

            //set recyclerView
            recyclerView.setHasFixedSize(true)
            recyclerView.layoutManager = LinearLayoutManager(activity)

            //set adapter to recyclerView
            recyclerView.adapter = adapter
        }
    }

    class SpinnerOnItemSelectedListener(private val fragment: CustomerProductListFragment, private val ownerId:String, private val categories: ArrayList<Category>): AdapterView.OnItemSelectedListener {
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
            categories[position].id?.let { it ->
                fragment.loadData(ownerId, it)
            }
        }

        override fun onNothingSelected(p0: AdapterView<*>?) {
            TODO("Not yet implemented")
        }
    }
}