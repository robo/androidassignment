package com.example.androidassignment.controller.owner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.androidassignment.R
import com.example.androidassignment.viewmodel.SharedViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OwnerDashboardFragment : Fragment() {
    companion object {
        //private val TAG = DashboardOwnerFragment::javaClass.name
    }

    private val model: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val layout = inflater.inflate(R.layout.fragment_owner_dashboard, container, false)

        layout.findViewById<CardView>(R.id.dashboard_owner_grid_profile)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardOwnerFragment_to_ownerProfileFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_owner_grid_orders)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardOwnerFragment_to_ownerOrdersTabFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_owner_grid_add_product)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardOwnerFragment_to_addProductFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_owner_grid_products)
            .setOnClickListener{
                findNavController()
                    .navigate(R.id.action_dashboardOwnerFragment_to_ownerProductListFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_owner_grid_riders_chat)
            .setOnClickListener{
                val action = OwnerDashboardFragmentDirections.actionDashboardOwnerFragmentToUserListFragment()

                findNavController()
                    .navigate(action)
            }

        layout.findViewById<CardView>(R.id.dashboard_owner_grid_logout)
            .setOnClickListener {
                model.signOut()

                model.currentUser.observe(viewLifecycleOwner) {
                    if (it.id == null) {
                        val action = (R.id.action_global_startFragment)

                        findNavController()
                            .navigate(action)
                    }
                }
            }

        return layout
    }
}