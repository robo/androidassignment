package com.example.androidassignment.controller.common

import androidx.fragment.app.activityViewModels
import com.example.androidassignment.viewmodel.SharedViewModel

abstract class SignUpFragment(fragmentViewId: Int) : ImageFragment(fragmentViewId) {
    companion object {
        const val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})"
    }

    val model: SharedViewModel by activityViewModels()
    protected open val selectedRole = ""

    abstract fun registerUser()
}