package com.example.androidassignment.controller.owner

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.EditText
import androidx.navigation.fragment.findNavController
import com.example.androidassignment.R
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.controller.common.SignUpFragment
import com.example.androidassignment.model.User
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class OwnerSignUpFragment : SignUpFragment(R.layout.fragment_owner_sign_up) {
    companion object {
        private val TAG = OwnerSignUpFragment::class.java.name
    }

    private lateinit var editEmail: EditText
    private lateinit var editConfirmEmail: EditText
    private lateinit var editPassword: EditText
    private lateinit var editConfirmPassword: EditText
    private lateinit var editFirstName: EditText
    private lateinit var editLastName: EditText
    private lateinit var editPhoneNumber: EditText
    private lateinit var editMinimarketName: EditText
    private lateinit var editAddress: AutoCompleteTextView

    override val selectedRole = "owner"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageView = view.findViewById(R.id.owner_sign_up_image_view)
        imageView
            .setOnClickListener {
                context?.let { context ->
                    selectImage(context)
                }
            }

        editFirstName = view.findViewById(R.id.owner_sign_up_first_name)
        editLastName = view.findViewById(R.id.owner_sign_up_last_name)
        editPhoneNumber = view.findViewById(R.id.owner_sign_up_phone_number)
        editMinimarketName = view.findViewById(R.id.owner_sign_up_minimarket_name)
        editAddress = view.findViewById(R.id.owner_sign_up_address)
        editEmail = view.findViewById(R.id.owner_sign_up_email)
        editConfirmEmail = view.findViewById(R.id.owner_sign_up_confirm_email)

        editPassword = view.findViewById(R.id.owner_sign_up_password)
        editConfirmPassword = view.findViewById(R.id.owner_sign_up_confirm_password)

        view.findViewById<Button>(R.id.owner_sign_up_button_submit)
            .setOnClickListener { registerUser() }
    }

    override fun registerUser() {
        val firstName = editFirstName.text.toString().trim()
        val lastName = editLastName.text.toString().trim()
        val phoneNumber = editPhoneNumber.text.toString().trim()
        val minimarketName = editMinimarketName.text.toString().trim()
        val address = editAddress.text.toString().trim()
        val email = editEmail.text.toString().trim()
        val confirmEmail = editConfirmEmail.text.toString().trim()
        val password = editPassword.text.toString().trim()
        val confirmPassword = editConfirmPassword.text.toString().trim()

        if (checkForm(firstName, lastName, phoneNumber, minimarketName, address, email, confirmEmail, password, confirmPassword)) {
            val geopoint = model.geocode(address)
            val niceAddress = geopoint?.let { model.reverseGeocode(it) } ?: address

            GlobalScope.launch {
                if (model.signUp(email, password)) {
                    val pictureURL = FirestorePath.USERS + "/" + model.currentUserId
                    val newOwner = User(
                        model.currentUserId,
                        firstName,
                        lastName,
                        phoneNumber,
                        niceAddress,
                        geopoint,
                        minimarketName,
                        null,
                        pictureURL,
                        selectedRole,
                        null,
                        null,
                        null
                    )

                    if (model.saveCurrentUser(newOwner)
                        && model.saveData(pictureURL, getImageBytes())) {
                        withContext(Dispatchers.Main) {
                            val msg = "User successfully registered"

                            Log.i(TAG, msg)

                            Snackbar.make(requireView(), msg, Snackbar.LENGTH_SHORT)
                                .show()

                            val action = OwnerSignUpFragmentDirections.actionOwnerSignUpFragmentToDashboardOwnerFragment()

                            findNavController()
                                .navigate(action)
                        }
                    } else {
                        Log.i(TAG, getString(R.string.internal_error))

                        Snackbar.make(requireView(), getString(R.string.internal_error), Snackbar.LENGTH_SHORT)
                            .show()

                        /* TODO(cplrossi): maybe rollback */
                    }
                } else {
                    val msg = "Cannot register user"

                    Log.i(TAG, msg)

                    Snackbar.make(requireView(), msg, Snackbar.LENGTH_SHORT)
                }
            }
        } else {   //if something wrong with the form
            Snackbar.make(requireView(), getString(R.string.please_double_check_data), Snackbar.LENGTH_SHORT)
                .show()
        }
    }

    private fun checkForm(
        firstName: String,
        lastName: String,
        phoneNumber: String,
        minimarketName: String,
        address: String,
        email: String,
        confirmEmail: String,
        password: String,
        confirmPassword: String
    ): Boolean {
        return if (firstName.isEmpty()) {
            editFirstName.error = getString(R.string.field_mandatory)
            editFirstName.requestFocus()
            false
        }
        else if (lastName.isEmpty()) {
            editLastName.error = getString(R.string.field_mandatory)
            editLastName.requestFocus()
            false
        }
        else if (phoneNumber.isEmpty()) {
            editPhoneNumber.error = getString(R.string.field_mandatory)
            editPhoneNumber.requestFocus()
            false
        }
        else if (minimarketName.isEmpty()) {
            editMinimarketName.error = getString(R.string.field_mandatory)
            editMinimarketName.requestFocus()
            false
        }
        else if (address.isEmpty()) {
            editAddress.error = getString(R.string.field_mandatory)
            editAddress.requestFocus()
            false
        }
        else if (email.isEmpty()) {
            editEmail.error = getString(R.string.field_mandatory)
            editEmail.requestFocus()
            false
        }
        else if (!EMAIL_REGEX.toRegex().matches(email)) {
            editEmail.error = getString(R.string.invalid_email)
            editEmail.requestFocus()
            false
        }
        else if (confirmEmail.isEmpty()) {
            editConfirmEmail.error = getString(R.string.field_mandatory)
            editConfirmEmail.requestFocus()
            false
        }
        else if (editEmail.text.toString() != editConfirmEmail.text.toString()) {
            //Toast.makeText(activity, "$email != $confirmEmail", Toast.LENGTH_LONG).show()
            editEmail.error = getString(R.string.emails_do_not_match)
            editConfirmEmail.error = getString(R.string.emails_do_not_match)
            editEmail.requestFocus()
            false
        }
        else if (password.isEmpty()) {
            editPassword.error = getString(R.string.field_mandatory)
            editPassword.requestFocus()
            false
        }
        else if (confirmPassword.isEmpty()) {
            editConfirmPassword.error = getString(R.string.field_mandatory)
            editConfirmPassword.requestFocus()
            false
        }
        else {
            if (password.length < 6 || confirmPassword.length < 6) {
                editPassword.error = getString(R.string.provide_longer_password)
                editConfirmPassword.error = getString(R.string.provide_longer_password)
                editPassword.requestFocus()
                false
            }
            else if (editPassword.text.toString() == editConfirmPassword.text.toString()) {
                true
            }
            else {
                editPassword.error = getString(R.string.passwords_do_not_match)
                editConfirmPassword.error = getString(R.string.passwords_do_not_match)
                editPassword.requestFocus()
                false
            }
        }
    }
}