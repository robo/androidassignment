package com.example.androidassignment.controller.common

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.activityViewModels
import com.example.androidassignment.viewmodel.SharedViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment:
    SupportMapFragment(), OnMapReadyCallback {
    companion object {
        private val TAG = MapFragment::class.java.name
    }

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        getMapAsync(this)
    }

    private val model: SharedViewModel by activityViewModels()

    override fun onMapReady(map: GoogleMap?) {
        Log.i(TAG, "map ready")

        model.selectedUsers.observe(viewLifecycleOwner) {
            if (map != null) {
                map.clear()

                Log.i(TAG, "found ${it.size} users")

                for (user in it) {
                    if (user.coordinates != null) {
                        val lat = user.coordinates!!.latitude
                        val lng = user.coordinates!!.longitude

                        map.addMarker(MarkerOptions()
                            .position(LatLng(lat, lng)
                            )
                            .title(user.firstName + user.lastName)
                        )

                        map.moveCamera(CameraUpdateFactory.newLatLng(LatLng(lat, lng)))
                    }
                }
            }
        }
    }
}