package com.example.androidassignment.controller.common

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassignment.R
import com.example.androidassignment.adapter.MessageAdapter
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.model.Conversation
import com.example.androidassignment.model.Message
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ChatFragment: Fragment(R.layout.fragment_chat) {
    companion object {
        private val TAG = ChatFragment::class.java.name
    }

    private val model: SharedViewModel by activityViewModels()
    private val firestore = Firebase.firestore

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.selectedChatRecipient.observe(viewLifecycleOwner) { otherUserId ->
            Log.i(TAG, "other user ID: $otherUserId")

            GlobalScope.launch {
                /* check if there is an history */
                var conversation = model.getConversationByUserIds(model.currentUserId, otherUserId)

                Log.i(TAG, conversation.toString())

                /* if not the case... */
                if (conversation == null) {
                    /* create conversation for the current user and the recipient */
                    conversation = Conversation(
                        null,
                        model.currentUserId,
                        otherUserId
                    )

                    if (!model.saveConversation(conversation)) {
                        withContext(Dispatchers.Main) {
                            Snackbar.make(requireView(), "Cannot start conversation", Snackbar.LENGTH_SHORT)
                                .show()
                        }
                    }
                }

                withContext(Dispatchers.Main) {
                    /* creating messages recycler view */
                    val query = firestore.collection(FirestorePath.MESSAGES)
                        .whereEqualTo("conversationId", conversation.id)
                        .orderBy("timestamp")
                        .limit(50)

                    val options = FirestoreRecyclerOptions.Builder<Message>()
                        .setQuery(query, Message::class.java)
                        .setLifecycleOwner(viewLifecycleOwner)
                        .build()

                    val recyclerView: RecyclerView = view.findViewById(R.id.chat_recycler_view)
                    recyclerView.layoutManager = LinearLayoutManager(context)
                    recyclerView.adapter = MessageAdapter(options)

                    /* setting up send button click listener */
                    model.currentUser.observe(viewLifecycleOwner) { currentUser ->
                        view.findViewById<ImageButton>(R.id.chat_button_send)
                            .setOnClickListener {
                                val messageEditText = view.findViewById<TextView>(R.id.chat_write_message)
                                val message = messageEditText.text.toString()

                                GlobalScope.launch {
                                    model.saveMessage(Message(
                                        null,
                                        model.currentUserId,
                                        conversation.id,
                                        Timestamp.now(),
                                        message,
                                        currentUser.firstName
                                    ))
                                }

                                messageEditText.text = ""
                            }
                    }


                }
            }
        }
    }
}