package com.example.androidassignment.controller.rider

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.androidassignment.R
import com.example.androidassignment.viewmodel.SharedViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class RiderDashboardFragment : Fragment(R.layout.fragment_rider_dashboard) {
    companion object {
        private val TAG = RiderDashboardFragment::javaClass.name
    }

    private var active: Boolean = true
    private val model: SharedViewModel by activityViewModels()

    @SuppressLint("ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val layout = inflater.inflate(R.layout.fragment_rider_dashboard, container, false)

        layout.findViewById<CardView>(R.id.dashboard_rider_grid_profile)
            .setOnClickListener {
                findNavController().navigate(R.id.action_dashboardRiderFragment_to_riderProfileFragment)
            }

        model.currentUser.observe(viewLifecycleOwner){ currentUser ->
            val statusImageView = layout.findViewById<ImageView>(R.id.dashboard_rider_status_image)
            layout.findViewById<CardView>(R.id.dashboard_rider_grid_status)
                .setOnClickListener{
                    val statusTextView = layout.findViewById<TextView>(R.id.dashboard_rider_status_text)
                    Log.i(TAG, "${statusTextView.text}")

                    if(currentUser.available == true){
                        Log.i(TAG, "was available, now unavailable")
                        statusTextView.setText(R.string.unavailable)
                        statusImageView.setImageResource(R.drawable.ic_rider_active)
                        active = false
                        currentUser.available = false
                        Snackbar.make(layout, "You are now unavailable.", Snackbar.LENGTH_SHORT).show()
                    }
                    else{
                        Log.i(TAG, "was unavailable, now available")
                        statusTextView.setText(R.string.available)
                        statusImageView.setImageResource(R.drawable.ic_rider_inactive)
                        active = true
                        currentUser.available = true
                        Snackbar.make(layout, "You are now available.", Snackbar.LENGTH_SHORT).show()
                    }

                    GlobalScope.launch {
                        if (!model.saveCurrentUser(currentUser)){
                            withContext(Dispatchers.Main){
                                Snackbar.make(layout, "An internal error has occurred.", Snackbar.LENGTH_SHORT).show()
                            }
                        }
                    }
                }

            layout.findViewById<CardView>(R.id.dashboard_rider_grid_boss_chat)
                .setOnClickListener {
                    currentUser.ownerId?.let { ownerId -> model.selectChatRecipient(ownerId) }

                    val action = RiderDashboardFragmentDirections.actionDashboardRiderFragmentToChatFragment()

                    findNavController()
                        .navigate(action)
                }
        }

        layout.findViewById<CardView>(R.id.dashboard_rider_grid_delivery_details)
            .setOnClickListener {
                findNavController()
                    .navigate(R.id.action_dashboardRiderFragment_to_riderOrdersTabFragment)
            }

        layout.findViewById<CardView>(R.id.dashboard_rider_grid_customer_chat)
            .setOnClickListener {
                val action = RiderDashboardFragmentDirections.actionDashboardRiderFragmentToUserListFragment()

                findNavController()
                    .navigate(action)
            }

        layout.findViewById<CardView>(R.id.dashboard_rider_grid_logout)
            .setOnClickListener {
                model.signOut()
                Snackbar.make(layout, "Signed out", Snackbar.LENGTH_SHORT).show()

                model.currentUser.observe(viewLifecycleOwner) {
                    if (it.id == null) {
                        val action = (R.id.action_global_startFragment)

                        findNavController()
                            .navigate(action)
                    }
                }
            }

        return layout
    }
}