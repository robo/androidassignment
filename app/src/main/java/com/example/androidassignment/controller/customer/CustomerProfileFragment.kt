package com.example.androidassignment.controller.customer

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.example.androidassignment.R
import com.example.androidassignment.controller.common.ProfileFragment
import com.example.androidassignment.model.User
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class CustomerProfileFragment : ProfileFragment(R.layout.fragment_customer_profile) {
    private lateinit var firstNameEditText: EditText
    private lateinit var lastNameEditText: EditText
    private lateinit var phoneNumberEditText: EditText
    private lateinit var addressEditText: EditText
    private lateinit var buttonSubmit: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageView = view.findViewById(R.id.customer_profile_image_view)
        imageView.setOnClickListener {
            context?.let { context -> selectImage(context) }
        }

        firstNameEditText = view.findViewById(R.id.customer_profile_first_name)
        lastNameEditText = view.findViewById(R.id.customer_profile_last_name)
        phoneNumberEditText = view.findViewById(R.id.customer_profile_phone_number)
        addressEditText = view.findViewById(R.id.customer_profile_address)
        buttonSubmit = view.findViewById(R.id.customer_profile_button_submit)

        loadProfileImage()

        model.currentUser.observe(viewLifecycleOwner){ user ->
            firstNameEditText.setText(user.firstName)
            lastNameEditText.setText(user.lastName)
            phoneNumberEditText.setText(user.phoneNumber)
            addressEditText.setText(user.coordinates?.let { model.reverseGeocode(it) })
            buttonSubmit.setOnClickListener {
                updateProfile(user)
            }
        }
    }

    override fun updateProfile(user: User) {
        GlobalScope.launch {
            val address = addressEditText.text.toString()
            val geopoint = model.geocode(address)
            val niceAddress = geopoint?.let { model.reverseGeocode(it) } ?: address

            val currentUser = User(
                user.id,
                firstNameEditText.text.toString(),
                lastNameEditText.text.toString(),
                phoneNumberEditText.text.toString(),
                niceAddress,
                geopoint,
                user.minimarket,
                user.ownerId,
                user.pictureURL,
                user.role,
                user.averageRating,
                user.numberOfRatings,
                user.available
            )

            if (model.saveCurrentUser(currentUser)) {
                if (hasUploadedImage) {
                    user.pictureURL?.let { url -> model.saveData(url, getImageBytes()) }
                }

                withContext(Dispatchers.Main){
                    Snackbar.make(requireView(), getString(R.string.profile_successfully_updated), Snackbar.LENGTH_SHORT)
                        .show()
                }
            } else {
                withContext(Dispatchers.Main){
                    Snackbar.make(requireView(), getString(R.string.internal_error), Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }
}