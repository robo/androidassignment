package com.example.androidassignment.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.GeoPoint

data class User(
    @DocumentId
    override var id: String?,

    var firstName: String?,
    var lastName: String?,
    var phoneNumber: String?,
    var address: String?,
    var coordinates: GeoPoint?,
    var minimarket: String?,
    var ownerId: String?,
    var pictureURL: String?,
    var role: String?,
    var averageRating: Float?,
    var numberOfRatings: Int?,
    var available: Boolean?
): Identifiable {
    //initialize empty constructor for Firebase use
    constructor() :
            this(null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            )
}
