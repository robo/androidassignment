package com.example.androidassignment.model

import com.google.firebase.firestore.DocumentId

data class Conversation(
    @DocumentId
    override var id: String?,

    var userId1: String? = "",
    var userId2: String? = ""
): Identifiable {
    constructor(): this (
        null,
        null,
        null
    )
}