package com.example.androidassignment.model

import com.google.firebase.firestore.DocumentId

data class OrderItem(
    @DocumentId
    override var id: String?,
    var productId: String?,
    var orderId: String?,
    var quantity: Int?,
    var subtotal: Float?
): Identifiable {
    constructor() : this(
        null,
        null,
        null,
        null,
        null
    )
}
