package com.example.androidassignment.model

import com.google.firebase.firestore.DocumentId

data class Category(
    @DocumentId
    override var id: String?,

    var name: String?
): Identifiable {
    constructor(): this(
        null,
        null
    )
}