package com.example.androidassignment.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId
import java.util.Date

data class Order(
    @DocumentId
    override var id: String?,

    var customerId: String?,
    var ownerId: String?,
    var riderId: String?,
    var statusCode: String?,
    var creationDate: Timestamp?,   //quando viene confermato l'ordine dal customer
    var placementDate: Timestamp?,  //quando viene assegnato al rider
    var details: String?,
    var totalPrice: Float?,
    var isRated: Boolean?
): Identifiable {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    )
}
