package com.example.androidassignment.model

interface Identifiable {
    var id: String?
}