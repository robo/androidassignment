package com.example.androidassignment.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId
import java.util.*

data class Message(
    @DocumentId
    override var id: String?,

    var userId: String? = "",
    var conversationId: String? = "",
    var timestamp: Timestamp? = Timestamp(Date(0)),
    var message: String? = "",
    var name: String? = ""

/* TODO(cplrossi): add pictureURL */
): Identifiable {
    constructor(): this(
        null,
        null,
        null,
        null,
        null,
        null
    )
}
