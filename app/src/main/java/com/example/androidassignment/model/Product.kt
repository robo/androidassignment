package com.example.androidassignment.model

import com.google.firebase.firestore.DocumentId

data class Product(
    @DocumentId
    override var id: String?,

    var ownerId: String?,
    var categoryId: String?,
    var name: String?,
    var price: Float?,
    var description: String?,
    var pictureURL: String?,
    var availableQuantity: Int?
): Identifiable {
    constructor() : this(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)
}
