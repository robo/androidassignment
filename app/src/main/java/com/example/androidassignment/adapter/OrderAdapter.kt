package com.example.androidassignment.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.model.Order
import com.example.androidassignment.model.OrderItem
import com.example.androidassignment.model.User
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.paging.FirestorePagingAdapter
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.firebase.ui.firestore.paging.LoadingState
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class OrderAdapter(options: FirestorePagingOptions<Order>,
                   private var activity: AppCompatActivity,
                   private var user: LiveData<User>,
                   private val onClickPickRiderListener: (Order) -> Unit,
                   private val onClickTrackListener: (Order) -> Unit
) : FirestorePagingAdapter<Order, OrderAdapter.OrderViewHolder>(options){

    companion object {
        private val TAG = OrderAdapter::class.java.name
    }

    private val vModel: SharedViewModel by activity.viewModels()

    private var parent: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        Log.i(TAG, "onCreateViewHolder")
        this.parent = parent
        return OrderViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.item_order, parent, false),
                    activity,
                    user
        )
    }

    //model is a populated Product - its data has already been retrieved
    override fun onBindViewHolder(holder: OrderViewHolder, position: Int, model: Order) {
        Log.i(TAG, "onBindViewHolder")
        holder.bind(model)

        /*double dispatcher first phase*/
        holder.itemOrderPickRiderButton.setOnClickListener {
            onClickPickRiderListener(model)
        }

        holder.itemOrderTrackRiderButton.setOnClickListener {
            onClickTrackListener(model)
        }

        holder.itemOrderSeeDestinationOnMapButton.setOnClickListener {
            onClickTrackListener(model)
        }

        holder.itemOrderAcceptButton.setOnClickListener {
            onAccept(model, holder.itemView)
        }

        holder.itemOrderDeclineButton.setOnClickListener {
            onDecline(model, holder.itemView)
        }

        holder.itemOrderMissionAccomplishedButton.setOnClickListener {
            onMissionAccomplished(model, holder.itemView)
        }

        holder.itemOrderMissionFailedButton.setOnClickListener {
            onMissionFailed(model, holder.itemView)
        }
    }

    override fun onLoadingStateChanged(state: LoadingState) {
        when (state) {
            LoadingState.LOADING_INITIAL -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.order_list_swipe_refresh)?.isRefreshing =
                    true
            }

            LoadingState.LOADING_MORE -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.order_list_swipe_refresh)?.isRefreshing =
                    true
            }

            LoadingState.LOADED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.order_list_swipe_refresh)?.isRefreshing =
                    false
            }

            LoadingState.ERROR -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.order_list_swipe_refresh)?.isRefreshing =
                    false
            }

            LoadingState.FINISHED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.order_list_swipe_refresh)?.isRefreshing =
                    false
            }
        }
    }


    private fun onAccept(order: Order, view: View) {
        GlobalScope.launch {
            val placementDate = Timestamp.now()

            Log.i(TAG, placementDate.toString())

            val acceptedOrder = Order(
                order.id,
                order.customerId,
                order.ownerId,
                order.riderId,
                "assigned",
                order.creationDate,
                placementDate,
                order.details,
                order.totalPrice,
                order.isRated
            )

            if (vModel.saveOrder(acceptedOrder)) {
                Snackbar
                    .make(view, "Order accepted! Please refresh to update this list!", Snackbar.LENGTH_SHORT)
                    .show()
            } else {
                Log.i(TAG, "failed accepting order")
            }
        }
    }

    private fun onDecline(order: Order, view: View) {
        GlobalScope.launch {
            val declinedOrder = Order(
                order.id,
                order.customerId,
                order.ownerId,
                null,
                "confirmed",
                order.creationDate,
                null,
                order.details,
                order.totalPrice,
                order.isRated
            )

            if (vModel.saveOrder(declinedOrder)) {
                Snackbar
                    .make(view, "Order declined! Please refresh to update this list!", Snackbar.LENGTH_SHORT)
                    .show()
            } else {
                Log.i(TAG, "failed declining order")
            }
        }
    }

    private fun onMissionAccomplished(order: Order, view: View){
        GlobalScope.launch {
            val deliveredOrder = Order(
                order.id,
                order.customerId,
                order.ownerId,
                order.riderId,
                "delivered",
                order.creationDate,
                order.placementDate,
                order.details,
                order.totalPrice,
                order.isRated
            )

            if(vModel.saveOrder(deliveredOrder)) {
                Snackbar
                    .make(view, "Order delivered! Please refresh to update this list!", Snackbar.LENGTH_SHORT)
                    .show()
            } else {
                Log.i(TAG, "failed delivering order")
            }
        }
    }

    private fun onMissionFailed(order: Order, view: View) {
        GlobalScope.launch {
            val deliveredOrder = Order(
                order.id,
                order.customerId,
                order.ownerId,
                order.riderId,
                "confirmed",
                order.creationDate,
                null,
                order.details,
                order.totalPrice,
                order.isRated)

            if (vModel.saveOrder(deliveredOrder)) {
                Snackbar
                    .make(view, "Order delivered! Please refresh to update this list!", Snackbar.LENGTH_SHORT)
                    .show()
            } else {
                Log.i(TAG, "failed delivering order")
            }
        }
    }

    //viewHolder for Product objects
    class OrderViewHolder(itemView: View, private var activity: AppCompatActivity, private var user: LiveData<User>) : RecyclerView.ViewHolder(itemView){
        companion object {
            private val TAG: String = OrderViewHolder::class.java.name
        }

        private val db = Firebase.firestore

        private val itemOrderId: TextView = itemView.findViewById(R.id.item_order_id)
        private val itemOrderCreationDate: TextView = itemView.findViewById(R.id.item_order_creation_date)
        private val itemOrderPlacementDate: TextView = itemView.findViewById(R.id.item_order_placement_date)
        private val itemOrderStatus: TextView = itemView.findViewById(R.id.item_order_status)
        private val itemOrderTotalPrice: TextView = itemView.findViewById(R.id.item_order_total_price)
        private val itemOrderRecyclerView: RecyclerView = itemView.findViewById(R.id.item_order_recycler_view)
//        private val itemOrderData: TextView = itemView.findViewById(R.id.item_order_data)
        val itemOrderRateRiderButton: Button = itemView.findViewById(R.id.item_order_rate_rider_button)
        val itemOrderPickRiderButton: Button = itemView.findViewById(R.id.item_order_pick_rider_button)
        val itemOrderTrackRiderButton: Button = itemView.findViewById(R.id.item_order_track_rider_button)
        val itemOrderAcceptButton: Button = itemView.findViewById(R.id.item_order_accept_button)
        val itemOrderDeclineButton: Button = itemView.findViewById(R.id.item_order_decline_button)
        val itemOrderSeeDestinationOnMapButton: Button = itemView.findViewById(R.id.item_order_see_destination_on_map_button)
        val itemOrderMissionAccomplishedButton: Button = itemView.findViewById(R.id.item_order_mission_accomplished_button)
        val itemOrderMissionFailedButton: Button = itemView.findViewById(R.id.item_order_mission_failed_button)
        private var adapter: OrderItemAdapter? = null

        fun bind(order: Order) {
            Log.i(TAG, "bind")
            itemOrderId.text = order.id
            itemOrderCreationDate.text = order.creationDate?.toDate().toString()
            itemOrderPlacementDate.text = order.placementDate?.toDate().toString()
            itemOrderStatus.text = order.statusCode

            user.observe(activity){
                when(it.role){
                    "owner" -> {
                        itemOrderPickRiderButton.isVisible = order.statusCode == "confirmed"
                        itemOrderTrackRiderButton.isVisible = order.statusCode == "assigned"
                        itemOrderAcceptButton.isVisible = false
                        itemOrderDeclineButton.isVisible = false
                        itemOrderSeeDestinationOnMapButton.isVisible = false
                        itemOrderMissionAccomplishedButton.isVisible = false
                        itemOrderMissionFailedButton.isVisible = false
                        itemOrderRateRiderButton.isVisible = false
                    }
                    "customer" -> {
                        itemOrderPickRiderButton.isVisible = false
                        itemOrderTrackRiderButton.isVisible = order.statusCode == "assigned"
                        itemOrderAcceptButton.isVisible = false
                        itemOrderDeclineButton.isVisible = false
                        itemOrderSeeDestinationOnMapButton.isVisible = false
                        itemOrderMissionAccomplishedButton.isVisible = false
                        itemOrderMissionFailedButton.isVisible = false
                        itemOrderRateRiderButton.isVisible = (order.statusCode == "delivered") && (order.isRated == false)
                    }
                    "rider" -> {
                        itemOrderPickRiderButton.isVisible = false
                        itemOrderTrackRiderButton.isVisible = false
                        itemOrderAcceptButton.isVisible = order.statusCode == "waiting"
                        itemOrderDeclineButton.isVisible = order.statusCode == "waiting"
                        itemOrderSeeDestinationOnMapButton.isVisible = order.statusCode == "assigned"
                        itemOrderMissionAccomplishedButton.isVisible = order.statusCode == "assigned"
                        itemOrderMissionFailedButton.isVisible = order.statusCode == "assigned"
                        itemOrderRateRiderButton.isVisible = false
                    }
                }
            }

            val query: Query = db.collection("orderItems").whereEqualTo("orderId", order.id)

            val config = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(10)
                .setPageSize(20)
                .build()

            val options: FirestorePagingOptions<OrderItem> = FirestorePagingOptions.Builder<OrderItem>()
                .setLifecycleOwner(activity)
                .setQuery(query, config, OrderItem::class.java)
                .build()

            adapter = OrderItemAdapter(options)

            itemOrderRecyclerView.apply {
                layoutManager = LinearLayoutManager(itemOrderRecyclerView.context)
                adapter = OrderItemAdapter(options)
                setRecycledViewPool(recycledViewPool)
            }

            //total price
            itemOrderTotalPrice.text = order.totalPrice.toString()
        }
    }
}
