package com.example.androidassignment.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.model.Product
import com.firebase.ui.firestore.paging.FirestorePagingAdapter
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.firebase.ui.firestore.paging.LoadingState
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import io.github.rosariopfernandes.firecoil.load
import java.text.DecimalFormat


class ProductAdapter(
    options: FirestorePagingOptions<Product>,
    private val onClickListener: (Product) -> Unit
): FirestorePagingAdapter<Product, ProductAdapter.ProductViewHolder> (options) {

    companion object {
        private val TAG = ProductAdapter::class.java.name
    }

    private var parent: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        Log.i(TAG, "onCreateViewHolder")
        this.parent = parent
        return ProductViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false))
    }

    //model is a populated Product - its data has already been retrieved
    override fun onBindViewHolder(holder: ProductViewHolder, position: Int, product: Product) {
        Log.i(TAG, "onBindViewHolder")

        //bind the model to the holder
        holder.bind(product)

        Log.i(TAG, product.pictureURL.toString())

        //get a reference to the pictureURL stored in the model
        //use FireCoil to load image on imageView

        val storageImageReference = Firebase.storage.reference.child(product.pictureURL.toString())
        Log.i(TAG, "image fetched, loading into imageView")
        holder.itemView.findViewById<ImageView>(R.id.item_product_image).load(storageImageReference){
            crossfade(true)
            placeholder(R.drawable.spinner)
            error(R.drawable.not_found)
        }

        holder.itemView.setOnClickListener {
            onClickListener(product)
        }
    }

    override fun onLoadingStateChanged(state: LoadingState) {
        when(state){
            LoadingState.LOADING_INITIAL -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.product_list_swipe_refresh)?.isRefreshing = true
            }

            LoadingState.LOADING_MORE -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.product_list_swipe_refresh)?.isRefreshing = true
            }

            LoadingState.LOADED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.product_list_swipe_refresh)?.isRefreshing = false
            }

            LoadingState.ERROR -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.product_list_swipe_refresh)?.isRefreshing = false
            }

            LoadingState.FINISHED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.product_list_swipe_refresh)?.isRefreshing = false
            }
        }
    }

    //viewHolder for Product objects
    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        companion object {
            private val TAG = ProductViewHolder::class.java.name
        }

        private val db = Firebase.firestore

        private val itemProductCategory: TextView = itemView.findViewById(R.id.item_product_category)
        private val itemProductName: TextView = itemView.findViewById(R.id.item_product_name)
        private val itemProductPrice: TextView = itemView.findViewById(R.id.item_product_price)

        fun bind(product: Product){
            Log.i(TAG, "bind")

            itemProductName.text = product.name

            product.categoryId?.let {
                db.collection("categories").document(it).get()
                    .addOnSuccessListener { document ->
                        if(document != null) {
                            itemProductCategory.text = document.data?.get("name").toString()
                            Log.i(TAG, "retrieved: ${document.data?.get("name")}")
                        } else{
                            Log.i(TAG, "document does not exist")
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.i(TAG, "error fetching category", exception)
                        itemProductCategory.text = product.categoryId
                    }
            }

            val price = product.price
            val df = DecimalFormat("#.##")

//            Log.i(TAG, "${df.format(price)}")

            itemProductPrice.text = df.format(price).toString()
            itemProductPrice.text = product.price.toString()
        }
    }
}