package com.example.androidassignment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.androidassignment.R
import com.example.androidassignment.model.Message
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class MessageAdapter(options: FirestoreRecyclerOptions<Message>)
    : FirestoreRecyclerAdapter<Message, MessageViewHolder>(options) {
    companion object {
//        private val TAG = MessageAdapter::class.java.name
        private const val MESSAGE_OUTGOING = 0xCAFE
        private const val MESSAGE_INCOMING = 0xBABE
    }

    private val auth = Firebase.auth

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return if (viewType == MESSAGE_OUTGOING) {
            val view = inflater.inflate(R.layout.fragment_chat_message_outgoing, parent, false)
            OutgoingMessageViewHolder(view)
        } else {
            val view = inflater.inflate(R.layout.fragment_chat_message_incoming, parent, false)
            IncomingMessageViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int, model: Message) {
        holder.bind(model)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isMe(position)) MESSAGE_OUTGOING
        else MESSAGE_INCOMING
    }

    private fun isMe(position: Int): Boolean {
        return snapshots[position].userId == auth.uid
    }

    class IncomingMessageViewHolder(itemView: View): MessageViewHolder(itemView) {
//        private val incomingProfileImage: ImageView = itemView.findViewById(R.id.chat_incoming_profile)
        private val incomingText: TextView = itemView.findViewById(R.id.chat_incoming_text)
        private val incomingName: TextView = itemView.findViewById(R.id.chat_incoming_name)

        override fun bind(message: Message) {
            incomingText.text = message.message
            incomingName.text = message.name
        }
    }

    class OutgoingMessageViewHolder(itemView: View): MessageViewHolder(itemView) {
//        private val outgoingProfileImage: ImageView = itemView.findViewById(R.id.chat_outgoing_profile)
        private val outgoingText: TextView = itemView.findViewById(R.id.chat_outgoing_text)

        override fun bind(message: Message) {
            outgoingText.text = message.message
        }
    }
}