package com.example.androidassignment.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.api.load
import com.example.androidassignment.R
import com.example.androidassignment.model.Product
import com.example.androidassignment.model.User
import com.example.androidassignment.viewmodel.SharedViewModel
import com.firebase.ui.firestore.paging.FirestorePagingAdapter
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.firebase.ui.firestore.paging.LoadingState
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import io.github.rosariopfernandes.firecoil.load
import java.lang.Exception


class MinimarketAdapter(
    options: FirestorePagingOptions<User>,
    private val onClickListener: (User) -> Unit,
    private val adapterFilter: (View, User) -> Unit
): FirestorePagingAdapter<User, MinimarketAdapter.MinimarketViewHolder>(options) {

    private val TAG: String = this.javaClass.simpleName
    private var parent: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MinimarketViewHolder {
        Log.i(TAG, "onCreateViewHolder")
        this.parent = parent
        return MinimarketViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_minimarket, parent, false))
    }

    //model is a populated User (owner) - its data has already been retrieved
    override fun onBindViewHolder(holder: MinimarketViewHolder, position: Int, model: User) {
        Log.i(TAG, "onBindViewHolder")

        //bind the model to the holder
        holder.bind(model)

        adapterFilter(holder.itemView, model)

        holder.itemView.setOnClickListener{
            onClickListener(model)
        }
    }

    override fun onLoadingStateChanged(state: LoadingState) {
        when(state){
            LoadingState.LOADING_INITIAL -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.customer_minimarket_list_swipe_refresh)?.isRefreshing = true
            }

            LoadingState.LOADING_MORE -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.customer_minimarket_list_swipe_refresh)?.isRefreshing = true
            }

            LoadingState.LOADED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.customer_minimarket_list_swipe_refresh)?.isRefreshing = false
            }

            LoadingState.ERROR -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.customer_minimarket_list_swipe_refresh)?.isRefreshing = false
            }

            LoadingState.FINISHED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.customer_minimarket_list_swipe_refresh)?.isRefreshing = false
            }
        }
    }

    //viewHolder for User (owner) objects
    class MinimarketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val TAG: String = this.javaClass.simpleName
        private val itemMinimarketName: TextView = itemView.findViewById(R.id.item_minimarket_name)
        private val itemMinimarketFirstName: TextView = itemView.findViewById(R.id.item_minimarket_first_name)
        private val itemMinimarketLastName: TextView = itemView.findViewById(R.id.item_minimarket_last_name)
        private val itemMinimarketAddress: TextView = itemView.findViewById(R.id.item_minimarket_address)
        private val itemMinimarketPhoneNumber: TextView = itemView.findViewById(R.id.item_minimarket_phone_number)
        private val itemMinimarketImageView: ImageView = itemView.findViewById(R.id.item_minimarket_image)

        private val storage = Firebase.storage

        fun bind(owner: User){
            itemMinimarketName.text = owner.minimarket
            itemMinimarketFirstName.text = owner.firstName
            itemMinimarketLastName.text = owner.lastName
            itemMinimarketAddress.text = owner.address
            itemMinimarketPhoneNumber.text = owner.phoneNumber


            if(owner.pictureURL.toString() != "" || owner.pictureURL != null) {
                val storageImageReference = storage.reference.child(owner.pictureURL.toString())
                itemMinimarketImageView.load(storageImageReference) {
                    crossfade(true)
                    placeholder(R.drawable.spinner)
                    error(R.drawable.not_found)
                }
            }
            else{
                itemMinimarketImageView.load(R.drawable.not_found) {
                    crossfade(true)
                    placeholder(R.drawable.spinner)
                    error(R.drawable.not_found)
                }
            }
        }
    }
}