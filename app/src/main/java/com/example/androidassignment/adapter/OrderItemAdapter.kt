package com.example.androidassignment.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.androidassignment.R
import com.example.androidassignment.model.OrderItem
import com.example.androidassignment.model.Product
import com.firebase.ui.firestore.paging.FirestorePagingAdapter
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.firebase.ui.firestore.paging.LoadingState
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import io.github.rosariopfernandes.firecoil.load

class OrderItemAdapter(options: FirestorePagingOptions<OrderItem>):
    FirestorePagingAdapter<OrderItem, OrderItemAdapter.OrderItemViewHolder>(options) {

    companion object {
        private val TAG = OrderItemAdapter::class.java.name
    }

    private var parent: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderItemViewHolder {
        Log.i(TAG, "onCreateViewHolder")
        this.parent = parent

        return OrderItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_orderitem, parent, false))
    }

    //model is a populated Product - its data has already been retrieved
    override fun onBindViewHolder(holder: OrderItemViewHolder, position: Int, model: OrderItem) {
        Log.i(TAG, "onBindViewHolder")

        holder.bind(model)
    }


    override fun onError(e: Exception) {
        super.onError(e)

        Log.i("TestActivity", e.message.toString())
    }

    override fun onLoadingStateChanged(state: LoadingState) {
        when(state){
            LoadingState.LOADING_INITIAL -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.orderitem_list_swipe_refresh)?.isRefreshing = true
            }

            LoadingState.LOADING_MORE -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.orderitem_list_swipe_refresh)?.isRefreshing = true
            }

            LoadingState.LOADED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.orderitem_list_swipe_refresh)?.isRefreshing = false
            }

            LoadingState.ERROR -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.orderitem_list_swipe_refresh)?.isRefreshing = false
            }

            LoadingState.FINISHED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.orderitem_list_swipe_refresh)?.isRefreshing = false
            }
        }
    }

    //viewHolder for Product objects
    class OrderItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        companion object {
            private val TAG = OrderItemViewHolder::class.java.name
        }

        private val db = Firebase.firestore

        lateinit var product: Product

        private val itemOrderItemName: TextView = itemView.findViewById(R.id.item_orderitem_name)
        private val itemOrderItemPrice: TextView = itemView.findViewById(R.id.item_orderitem_price)
        private val itemOrderItemQuantity: TextView = itemView.findViewById(R.id.item_orderitem_quantity)
        private val itemOrderItemSubtotal: TextView = itemView.findViewById(R.id.item_orderitem_subtotal)

        fun bind(orderItem: OrderItem){
            Log.i(TAG, "bind")

            product = Product()
            orderItem.productId?.let { productId ->
                db.collection("products").document(productId).get()
                    .addOnSuccessListener { documentSnapshot ->
                        Log.i(TAG, "product fetch complete")
                        product = documentSnapshot.toObject(Product::class.java)!!
                        itemOrderItemName.text = product.name
                        itemOrderItemPrice.text = product.price.toString()
                        itemOrderItemQuantity.text = orderItem.quantity.toString()
                        itemOrderItemSubtotal.text = orderItem.subtotal.toString()

                        val storageImageReference = product.pictureURL?.let { pictureURL ->
                            Firebase.storage.reference.child(pictureURL)
                        }
                        Log.i(TAG, "image fetched, loading into imageView")
                        if (storageImageReference != null) {
                            itemView.findViewById<ImageView>(R.id.item_orderitem_image).load(storageImageReference){
                                crossfade(true)
                                placeholder(R.drawable.spinner)
                                error(R.drawable.not_found)
                            }
                        }
                    }
                    .addOnFailureListener {
                        Log.i(TAG, "failed fetching Product")
                    }
            }
        }
    }
}