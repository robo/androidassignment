package com.example.androidassignment.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.api.load
import com.example.androidassignment.R
import com.example.androidassignment.model.User
import com.firebase.ui.firestore.paging.FirestorePagingAdapter
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.firebase.ui.firestore.paging.LoadingState
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import io.github.rosariopfernandes.firecoil.load

class UserChatAdapter(
    options: FirestorePagingOptions<User>,
    private val onClickListener: (User) -> Unit
) :
    FirestorePagingAdapter<User, UserChatAdapter.UserChatViewHolder>(options) {
    companion object {
        private val TAG: String = this.javaClass.simpleName
    }
    private var parent: ViewGroup? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserChatViewHolder {
        Log.i(TAG, "onCreateViewHolder")
        this.parent = parent
        return UserChatViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_user_chat, parent, false))
    }

    //model is a populated User (owner) - its data has already been retrieved
    override fun onBindViewHolder(holder: UserChatViewHolder, position: Int, model: User) {
        Log.i(TAG, "onBindViewHolder")

        //bind the model to the holder
        holder.bind(model)

        holder.itemView.setOnClickListener {
            onClickListener(model)
        }
    }

    override fun onLoadingStateChanged(state: LoadingState) {
        when(state){
            LoadingState.LOADING_INITIAL -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.chat_user_list_swipe_refresh)?.isRefreshing = true
            }

            LoadingState.LOADING_MORE -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.chat_user_list_swipe_refresh)?.isRefreshing = true
            }

            LoadingState.LOADED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.chat_user_list_swipe_refresh)?.isRefreshing = false
            }

            LoadingState.ERROR -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.chat_user_list_swipe_refresh)?.isRefreshing = false
            }

            LoadingState.FINISHED -> {
                parent?.findViewById<SwipeRefreshLayout>(R.id.chat_user_list_swipe_refresh)?.isRefreshing = false
            }
        }
    }

    //viewHolder for User (rider) objects
    class UserChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        companion object {
            private val TAG: String = this.javaClass.simpleName
        }

        private val userChatImageView: ImageView = itemView.findViewById(R.id.item_user_chat_image)
        private val userChatFirstName: TextView = itemView.findViewById(R.id.item_user_chat_first_name)
        private val userChatLastName: TextView = itemView.findViewById(R.id.item_user_chat_last_name)
        private val userChatRole: TextView = itemView.findViewById(R.id.item_user_chat_role)

        private val storage = Firebase.storage

        fun bind(user: User){
            Log.i(TAG, "bind")
            userChatFirstName.text = user.firstName
            userChatLastName.text = user.lastName
            userChatRole.text = user.role

            Log.i(TAG, "pictureURL: ${user.pictureURL.toString()}")

            if(user.pictureURL.toString() != "" || user.pictureURL != null) {
                val storageImageReference = storage
                    .reference
                    .child(
                        user
                            .pictureURL
                            .toString()
                    )
                userChatImageView.load(storageImageReference) {
                    crossfade(true)
                    placeholder(R.drawable.spinner)
                    error(R.drawable.not_found)
                }
            }
            else{
                userChatImageView.load(R.drawable.not_found) {
                    crossfade(true)
                    placeholder(R.drawable.spinner)
                    error(R.drawable.not_found)
                }
            }
        }
    }
}