package com.example.androidassignment.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassignment.model.Message

abstract class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(message: Message)
}