package com.example.androidassignment.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.api.load
import com.example.androidassignment.R
import com.example.androidassignment.model.User
import com.firebase.ui.firestore.paging.FirestorePagingAdapter
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.firebase.ui.firestore.paging.LoadingState
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import io.github.rosariopfernandes.firecoil.load

class RiderAdapter(
    options: FirestorePagingOptions<User>,
    private val onClickListener: (User) -> Unit
) :
    FirestorePagingAdapter<User, RiderAdapter.RiderViewHolder>(options) {
        companion object {
            private val TAG = RiderAdapter::class.java.name
        }

        private var parent: ViewGroup? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RiderViewHolder {
            Log.i(TAG, "onCreateViewHolder")
            this.parent = parent
            return RiderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_rider, parent, false))
        }

        //model is a populated User (owner) - its data has already been retrieved
        override fun onBindViewHolder(holder: RiderViewHolder, position: Int, model: User) {
            Log.i(TAG, "onBindViewHolder")

            //bind the model to the holder
            holder.bind(model)

            holder.itemView.setOnClickListener {
                onClickListener(model)
            }
        }

        override fun onLoadingStateChanged(state: LoadingState) {
            when(state){
                LoadingState.LOADING_INITIAL -> {
                    parent?.findViewById<SwipeRefreshLayout>(R.id.rider_list_swipe_refresh)?.isRefreshing = true
                }

                LoadingState.LOADING_MORE -> {
                    parent?.findViewById<SwipeRefreshLayout>(R.id.rider_list_swipe_refresh)?.isRefreshing = true
                }

                LoadingState.LOADED -> {
                    parent?.findViewById<SwipeRefreshLayout>(R.id.rider_list_swipe_refresh)?.isRefreshing = false
                }

                LoadingState.ERROR -> {
                    parent?.findViewById<SwipeRefreshLayout>(R.id.rider_list_swipe_refresh)?.isRefreshing = false
                }

                LoadingState.FINISHED -> {
                    parent?.findViewById<SwipeRefreshLayout>(R.id.rider_list_swipe_refresh)?.isRefreshing = false
                }
            }
        }

        //viewHolder for User (rider) objects
        class RiderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            companion object {
                private val TAG = RiderViewHolder::class.java.name
            }

            private val itemRiderFirstName: TextView = itemView.findViewById(R.id.item_rider_first_name)
            private val itemRiderLastName: TextView = itemView.findViewById(R.id.item_rider_last_name)
            private val itemRiderAddress: TextView = itemView.findViewById(R.id.item_rider_address)
            private val itemRiderPhoneNumber: TextView = itemView.findViewById(R.id.item_rider_phone_number)
            private val itemRiderImageView: ImageView = itemView.findViewById(R.id.item_rider_image)
            private val itemRiderRatingBar: RatingBar = itemView.findViewById(R.id.item_rider_rating_bar)

            private val storage = Firebase.storage

            fun bind(rider: User){
                Log.i(TAG, "bind")
                itemRiderFirstName.text = rider.firstName
                itemRiderLastName.text = rider.lastName
                itemRiderAddress.text = rider.address
                itemRiderPhoneNumber.text = rider.phoneNumber
                itemRiderRatingBar.rating = if(rider.averageRating == null) {
                                                0f
                                            }
                                            else{
                                                rider.averageRating!!
                                            }

                Log.i(TAG, "pictureURL: ${rider.pictureURL.toString()}")

                if(rider.pictureURL.toString() != "" || rider.pictureURL != null) {
                    val storageImageReference = storage.reference.child(rider.pictureURL.toString())
                    itemRiderImageView.load(storageImageReference) {
                        crossfade(true)
                        placeholder(R.drawable.spinner)
                        error(R.drawable.not_found)
                    }
                }
                else{
                    itemRiderImageView.load(R.drawable.not_found) {
                        crossfade(true)
                        placeholder(R.drawable.spinner)
                        error(R.drawable.not_found)
                    }
                }
            }
        }
}