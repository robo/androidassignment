
package com.example.androidassignment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidassignment.model.*
import com.example.androidassignment.repo.*
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class SharedViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val userRepository: UserRepository,
    private val productRepository: ProductRepository,
    private val categoryRepository: CategoryRepository,
    private val orderRepository: OrderRepository,
    private val orderItemRepository: OrderItemRepository,
    private val geocodingRepository: GeocodingRepository,
    private val messageRepository: MessageRepository,
    private val conversationRepository: ConversationRepository
): ViewModel() {
    companion object {
//        private val TAG = SharedViewModel::class.java.name
    }

    val currentUser: LiveData<User> by lazy { authRepository.getInfo() }

    suspend fun sendVerificationEmail(): Boolean {
        return authRepository.sendVerificationEmail()
    }

    val isSignedIn
        get() = authRepository.isSignedIn()

    val currentUserId
        get() = authRepository.getUid()

    suspend fun saveCurrentUser(currentUser: User): Boolean {
        return userRepository.save(currentUser)
    }

    private val _selectedUser = MutableLiveData<User>()
    val selectedUser: LiveData<User>
    get() = _selectedUser

    fun selectUser(user: User){
        _selectedUser.postValue(user)
    }

    private val _selectedUsers = MutableLiveData<List<User>>()
    val selectedUsers: LiveData<List<User>>
    get() = _selectedUsers

    fun selectUsers(users: List<User>) {
        _selectedUsers.postValue(users)
    }

    fun getUser(id: String): LiveData<User> {
        return userRepository.get(id)
    }

    suspend fun getUserSync(id: String): User? {
        return userRepository.getSync(id)
    }

    fun getOwners(): LiveData<List<User>> {
        return userRepository.getByRole("owner")
    }

    fun getCustomers(): LiveData<List<User>> {
        return userRepository.getByRole("customer")
    }

    fun getRiders(): LiveData<List<User>> {
        return userRepository.getByRole("rider")
    }

    private var _selectedProduct = MutableLiveData<Product>()
    val selectedProduct: LiveData<Product>
    get() = _selectedProduct

    fun selectProduct(product: Product) {
        _selectedProduct.postValue(product)
    }

    private var _selectedOrder = MutableLiveData<Order>()
    val selectedOrder: LiveData<Order>
    get() = _selectedOrder

    fun selectOrder(order: Order){
        _selectedOrder.postValue(order)
    }

    suspend fun saveOrder(order: Order): Boolean{
        return orderRepository.save(order)
    }

    private var _selectedOrderItem = MutableLiveData<OrderItem>()
    val selectedOrderItem: LiveData<OrderItem>
        get() = _selectedOrderItem

    fun selectOrderItem(orderItem: OrderItem){
        _selectedOrderItem.postValue(orderItem)
    }

    suspend fun saveOrderItem(orderItem: OrderItem): Boolean {
        return orderItemRepository.save(orderItem)
    }

    var ordersCount: Int = -1

    fun getOrdersByOwnerId(ownerId: String): LiveData<List<Order>> {
        return orderRepository.getByOwnerId(ownerId)
    }

    fun getOrdersByCustomerIdByStatus(customerId: String, status: String): LiveData<List<Order>> {
        return orderRepository.getByCustomerIdByStatus(customerId, status)
    }

    fun getOrdersByOwnerIdByStatus(ownerId: String, status: String): LiveData<List<Order>> {
        return orderRepository.getByOwnerIdByStatus(ownerId, status)
    }

    fun getOrdersByRiderIdByStatus(riderId: String, status: String): LiveData<List<Order>> {
        return orderRepository.getByRiderIdByStatus(riderId, status)
    }

    suspend fun signUp(email: String, password: String): Boolean {
        return authRepository.signUp(email, password)
    }

    fun signOut() {
        authRepository.signOut()
    }

    fun getProduct(id: String): LiveData<Product> {
        return productRepository.get(id)
    }

    suspend fun saveProduct(product: Product): Boolean {
        return productRepository.save(product)
    }

    fun getCategories(): LiveData<List<Category>> {
        return categoryRepository.list()
    }

    fun getStorageReference(url: String): StorageReference {
        return Firebase.storage.reference.child(url)
    }

    /* just for small data... */
    suspend fun saveData(url: String, data: ByteArray): Boolean {
        val ref = getStorageReference(url)

        return try {
            ref.putBytes(data).await()

            true
        } catch (e: Exception) {
            false
        }
    }

    /* see above... */
    suspend fun getData(url: String): ByteArray {
        val ref = getStorageReference(url)
        val oneMegabyte: Long = 1024 * 1024

        return try {
            ref.getBytes(oneMegabyte).await()
        } catch (e: Exception) {
            ByteArray(0)
        }
    }

    fun geocode(address: String): GeoPoint? {
        return geocodingRepository.geocode(address)
    }

    fun reverseGeocode(geoPoint: GeoPoint): String? {
        return geocodingRepository.reverseGeocode(geoPoint)
    }

    suspend fun saveMessage(message: Message): Boolean {
        return messageRepository.save(message)
    }

    private var _selectedChatRecipient = MutableLiveData<String>()
    val selectedChatRecipient: LiveData<String>
    get() = _selectedChatRecipient

    fun selectChatRecipient(uid: String) {
        _selectedChatRecipient.postValue(uid)
    }

    suspend fun getConversationByUserIds(userId1: String, userId2: String): Conversation? {
        return conversationRepository.getByUserIdsSync(userId1, userId2)
    }

    suspend fun saveConversation(conversation: Conversation): Boolean {
        return conversationRepository.save(conversation)
    }
}