package com.example.androidassignment.repo.firestore

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.livedata.firestore.FirestoreDocumentLiveData
import com.example.androidassignment.livedata.firestore.FirestoreQueryLiveData
import com.example.androidassignment.model.Order
import com.example.androidassignment.repo.OrderRepository
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class FirestoreOrderRepository @Inject constructor(): OrderRepository {
    companion object {
//        private val TAG = FirestoreOrderRepository::class.java.name
    }

    private val firestore = Firebase.firestore
    private val ordersCollection = firestore.collection(FirestorePath.ORDERS)

    override fun get(id: String): LiveData<Order> {
        val orderDocRef = ordersCollection.document(id)
        val documentLiveData = FirestoreDocumentLiveData(orderDocRef)

        return Transformations.map(documentLiveData) {
            documentLiveData.value?.toObject(Order::class.java)
        }
    }

    override fun getByCustomerIdByStatus(customerId: String, status: String): LiveData<List<Order>> {
        val ordersDocRef = ordersCollection
            .whereEqualTo("customerId", customerId)
            .whereEqualTo("statusCode", status)
        val queryLiveData = FirestoreQueryLiveData(ordersDocRef)

        return Transformations.map(queryLiveData) {
            queryLiveData.value?.toObjects(Order::class.java)
        }
    }

    override fun getByOwnerIdByStatus(ownerId: String, status: String): LiveData<List<Order>> {
        val ordersDocRef = ordersCollection
            .whereEqualTo("ownerId", ownerId)
            .whereEqualTo("statusCode", status)
        val queryLiveData = FirestoreQueryLiveData(ordersDocRef)

        return Transformations.map(queryLiveData) {
            queryLiveData.value?.toObjects(Order::class.java)
        }
    }

    override fun getByRiderIdByStatus(riderId: String, status: String): LiveData<List<Order>> {
        val ordersDocRef = ordersCollection
            .whereEqualTo("riderId", riderId)
            .whereEqualTo("statusCode", status)
        val queryLiveData = FirestoreQueryLiveData(ordersDocRef)

        return Transformations.map(queryLiveData) {
            queryLiveData.value?.toObjects(Order::class.java)
        }
    }

    override fun getByOwnerId(ownerId: String): LiveData<List<Order>> {
        val ordersDocRef = ordersCollection.whereEqualTo("ownerId", ownerId)
        val queryLiveData = FirestoreQueryLiveData(ordersDocRef)

        return Transformations.map(queryLiveData) {
            queryLiveData.value?.toObjects(Order::class.java)
        }
    }

    override suspend fun save(order: Order): Boolean {
        if (order.id != null && order.id != "") {
            /* check existence, and, in case, that totalPrice is equal to previous value */

            /* user provided id */
            val t = order.id?.let { ordersCollection.document(it).set(order) }

            return try {
                t?.await()  // from kotlinx-coroutines-play-services
                true
            } catch (e: Exception) {
                false
            }
        } else {
            /* this is a new order */
            order.totalPrice = .0F

            /* auto-generated id */
            val t = ordersCollection.add(order)

            return try {
                /* updating domain object id */
                order.id = t.await().id
                true
            } catch (e: Exception) {
                false
            }
        }
    }

    override suspend fun delete(id: String): Boolean {
        /* delete also related order items */
        val orderItemsQuery = firestore.collection(FirestorePath.ORDER_ITEMS).whereEqualTo("orderId", id)
        val querySnapshot = orderItemsQuery.get().await()

        for (document in querySnapshot) {
            /* TODO(cplrossi): create id list */
        }

        return try {
            firestore.runTransaction { transaction ->
                /* TODO(cplrossi): implement delete of order and related order items */
            }

            true
        } catch (e: Exception) {
            false
        }
    }
}