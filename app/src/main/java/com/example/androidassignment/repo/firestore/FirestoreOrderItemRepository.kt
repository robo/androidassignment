package com.example.androidassignment.repo.firestore

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.livedata.firestore.FirestoreDocumentLiveData
import com.example.androidassignment.livedata.firestore.FirestoreQueryLiveData
import com.example.androidassignment.model.OrderItem
import com.example.androidassignment.repo.OrderItemRepository
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class FirestoreOrderItemRepository @Inject constructor(): OrderItemRepository {
    companion object {
        private val TAG = FirestoreOrderItemRepository::class.java.name
    }

    private val firestore = Firebase.firestore
    private val orderItemsCollection = firestore.collection(FirestorePath.ORDER_ITEMS)

    override fun get(id: String): LiveData<OrderItem> {
        val orderItemDocRef = orderItemsCollection.document(id)
        val documentLiveData = FirestoreDocumentLiveData(orderItemDocRef)

        return Transformations.map(documentLiveData) {
            documentLiveData.value?.toObject(OrderItem::class.java)
        }
    }

    override fun getByOrder(orderId: String): LiveData<List<OrderItem>> {
        val orderItemsQuery = orderItemsCollection.whereEqualTo("orderId", orderId)
        val queryLiveData = FirestoreQueryLiveData(orderItemsQuery)

        return Transformations.map(queryLiveData) {
            queryLiveData.value?.toObjects(OrderItem::class.java)
        }
    }

    override suspend fun save(orderItem: OrderItem): Boolean {
        return try {
            /* we assure subtotal field is correct and we must update atomically order total price
            * and product availability
            */
            firestore.runTransaction { transaction ->
                val productDocumentRef =
                    orderItem.productId?.let {
                        firestore.collection(FirestorePath.PRODUCTS).document(it)
                    }

                val orderDocumentRef =
                    orderItem.orderId?.let {
                        firestore.collection(FirestorePath.ORDERS).document(it)
                    }

                val orderItemDocumentRef = if (orderItem.id != null && orderItem.id != "") {
                    /* using user provided id */
                    orderItemsCollection.document(orderItem.id!!)
                } else {
                    /* let firestore generate an id */
                    orderItemsCollection.document()
                }

                val productSnapshot = productDocumentRef?.let { transaction.get(it) }
                Log.i(TAG, "got product")

                val orderSnapshot = orderDocumentRef?.let { transaction.get(it) }
                Log.i(TAG, "got order")

                val orderItemSnapshot = if (orderItem.id != null && orderItem.id != "") {
                    orderItemDocumentRef.let { transaction.get(it) }
                } else null

                if(orderItemSnapshot != null) Log.i(TAG, "got orderItem")

                if (productSnapshot != null && orderSnapshot != null) {
                    /* update section of transaction */
                    val price = productSnapshot.getDouble("price")?.toFloat()

                    val oldSubtotal = if (orderItemSnapshot != null) {    /* order item already in db */
                        orderItemSnapshot.getDouble("subtotal")?.toFloat()!!
                    } else {                            /* new order item */
                        .0F
                    }

                    val newSubtotal = orderItem.quantity?.times(price!!)
                    val difference = newSubtotal?.minus(oldSubtotal)
                    val newTotal =
                        orderSnapshot.getDouble("totalPrice")?.toFloat()?.plus(difference!!)

                    /* setting calculated field in domain model object */
                    orderItem.subtotal = newSubtotal

                    /* decrement availability in product document */
                    val productAvailability = productSnapshot.getLong("availableQuantity")?.toInt()

                    if (productAvailability != null && productAvailability >= orderItem.quantity!!) {
                        transaction.update(productDocumentRef, "availableQuantity", productAvailability - orderItem.quantity!!)
                        Log.i(TAG, "availableQuantity updated")
                    } else throw FirebaseFirestoreException("no availability", FirebaseFirestoreException.Code.ABORTED)

                    /* updating calculated field (totalPrice) in order document */
                    transaction.update(orderDocumentRef, "totalPrice", newTotal)
                    Log.i(TAG, "totalPrice updated")

                    /* updating order item document with calculated subtotal */
                    transaction.set(orderItemDocumentRef, orderItem)
                    Log.i(TAG, orderItemDocumentRef.path)
                    Log.i(TAG, "orderItem set")
                } else throw FirebaseFirestoreException("no product", FirebaseFirestoreException.Code.ABORTED)

                /* success */
                null
            }.await()

            true
        } catch (e: Exception) {
            e.message?.let { Log.i(TAG, it) }
            false
        }
    }

    override suspend fun delete(id: String): Boolean {
        /* we must update atomically order total price */
        return try {

            firestore.runTransaction { transaction ->
                val orderItemDocumentRef = orderItemsCollection.document(id)
                val orderItemSnapshot = transaction.get(orderItemDocumentRef)
                val orderId = orderItemSnapshot.getString("orderId")
                val oldSubtotal = orderItemSnapshot.getDouble("subtotal")?.toFloat()
                val orderDocumentRef = orderId?.let {
                    firestore.collection(FirestorePath.ORDERS).document(it)
                }

                /* subtracting old subtotal from calculated field (totalPrice) in order document */
                val oldTotalPrice =
                    orderDocumentRef?.let { transaction.get(it).getDouble("totalPrice")?.toFloat() }
                val newTotalPrice = oldSubtotal?.let { oldTotalPrice?.minus(it) }

                if (orderDocumentRef != null) {
                    transaction.update(orderDocumentRef, "totalPrice", newTotalPrice)
                }

                /* TODO(cplrossi): aggiornare product availability */

                transaction.delete(orderItemDocumentRef)

                /* success */
                null
            }.await()

            true
        } catch (e: Exception) {
            false
        }
    }
}