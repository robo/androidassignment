package com.example.androidassignment.repo.firestore

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.livedata.firestore.FirestoreDocumentLiveData
import com.example.androidassignment.livedata.firestore.FirestoreQueryLiveData
import com.example.androidassignment.model.User
import com.example.androidassignment.repo.UserRepository
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class FirestoreUserRepository @Inject constructor(): UserRepository {
    companion object {
//        private val TAG = FirestoreUserRepository::class.java.name
    }

    private val firestore = Firebase.firestore
    private val usersCollection = firestore.collection(FirestorePath.USERS)

    override fun get(id: String): LiveData<User> {
        val userDocRef = usersCollection.document(id)
        val documentLiveData = FirestoreDocumentLiveData(userDocRef)

        return Transformations.map(documentLiveData) {
            documentLiveData.value?.toObject(User::class.java)
        }
    }

    override suspend fun getSync(id: String): User? {
        val userDocRef = usersCollection.document(id)

        return userDocRef.get().await().toObject(User::class.java)
    }

    override fun getByRole(role: String): LiveData<List<User>> {
        val collectionLiveData = FirestoreQueryLiveData(usersCollection.whereEqualTo("role", role))

        return Transformations.map(collectionLiveData) {
            collectionLiveData.value?.toObjects(User::class.java)
        }
    }


    override suspend fun save(user: User): Boolean {
        if (user.id != null && user.id != "") {
            /* user provided id */
            val t = user.id?.let { usersCollection.document(it).set(user) }

            return try {
                t?.await()
                true
            } catch (e: Exception) {
                false
            }
        } else {
            /* auto-generated id */
            val t = usersCollection.add(user)

            return try {
                user.id = t.await().id   // from kotlinx-coroutines-play-services
                true
            } catch (e: Exception) {
                false
            }
        }
    }

    override suspend fun delete(id: String): Boolean {
        val t = usersCollection.document(id).delete()

        return try {
            t.await()
            true
        } catch (e: Exception) {
            false
        }
    }
}