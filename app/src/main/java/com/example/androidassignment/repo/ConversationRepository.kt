package com.example.androidassignment.repo

import com.example.androidassignment.model.Conversation

interface ConversationRepository {
    suspend fun getByUserIdsSync(userId1: String, userId2: String): Conversation?
    suspend fun save(conversation: Conversation): Boolean
}