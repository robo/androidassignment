package com.example.androidassignment.repo.gcloud
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.util.Log
import com.example.androidassignment.repo.GeocodingRepository
import com.google.firebase.firestore.GeoPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.StringBuilder
import javax.inject.Inject

class GcloudGeocodingRepository @Inject constructor(@ApplicationContext private val context: Context): GeocodingRepository {
    companion object {
        private val TAG = GcloudGeocodingRepository::class.java.name
    }

    private val geocoder = Geocoder(context)

    override fun geocode(address: String): GeoPoint? {
        val res: Address

        try {
            res = geocoder.getFromLocationName(address, 1)[0]
        } catch (e: Exception) {
            Log.i(TAG, e.message ?: "exception")

            return null
        }

        return GeoPoint(res.latitude, res.longitude)
    }

    override fun reverseGeocode(geoPoint: GeoPoint): String? {
        val res: Address

        try {
            res = geocoder.getFromLocation(geoPoint.latitude, geoPoint.longitude, 1)[0]
        } catch (e: Exception) {
            Log.i(TAG, e.message ?: "exception")

            return null
        }

        return getSingleLineAddress(res)
    }

    private fun getSingleLineAddress(address: Address): String {
        val sb = StringBuilder()

        for (i in 0..address.maxAddressLineIndex) {
            sb.append(address.getAddressLine(i))
        }

        return sb.toString()
    }
}