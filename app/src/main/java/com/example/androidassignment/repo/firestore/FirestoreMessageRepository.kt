package com.example.androidassignment.repo.firestore

import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.model.Message
import com.example.androidassignment.repo.MessageRepository
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import javax.inject.Inject

class FirestoreMessageRepository @Inject constructor(): MessageRepository {
    companion object {
//        private val TAG = FirestoreMessageRepository::class.java.name
    }

    private val firestore = Firebase.firestore
    private val messagesCollection = firestore.collection(FirestorePath.MESSAGES)

    override suspend fun save(message: Message): Boolean {
        if (message.id != null && message.id != "") {
            /* user provided id */
            val t = message.id?.let { messagesCollection.document(it).set(message) }

            return try {
                t?.await()  // from kotlinx-coroutines-play-services
                true
            } catch (e: Exception) {
                false
            }
        } else {
            /* auto-generated id */
            val t = messagesCollection.add(message)

            return try {
                /* updating domain object id */
                message.id = t.await().id
                true
            } catch (e: Exception) {
                false
            }
        }
    }
}