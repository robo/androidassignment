package com.example.androidassignment.repo.auth

import androidx.lifecycle.LiveData
import com.example.androidassignment.livedata.auth.AuthLiveData
import com.example.androidassignment.model.User
import com.example.androidassignment.repo.AuthRepository
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class FirebaseAuthRepository @Inject constructor(): AuthRepository {
    companion object {
//        private val TAG = FirebaseAuthRepository::class.java.name
    }

    /* Firebase cloud dependency */
    private val auth = Firebase.auth

    private val currentUser by lazy { AuthLiveData() }

    override suspend fun sendVerificationEmail(): Boolean {
        try {
            auth.currentUser!!.sendEmailVerification().await()
        } catch (e: Exception) {
            return false
        }

        return false
    }

    override suspend fun signUp(email: String, password: String): Boolean {
        try {
            auth.createUserWithEmailAndPassword(email, password).await()
        } catch (e: Exception) {
            return false
        }

        return true
    }

    override fun isSignedIn(): Boolean {
        return auth.currentUser != null
    }

    override fun signOut() {
        auth.signOut()
    }

    override fun getUid(): String {
        return auth.currentUser?.uid ?: ""
    }

    override fun getLogin(): String {
        return auth.currentUser?.email ?: ""
    }

    override fun getInfo(): LiveData<User> {
        return currentUser
    }
}