package com.example.androidassignment.repo.firestore

import android.util.Log
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.model.Conversation
import com.example.androidassignment.repo.ConversationRepository
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class FirestoreConversationRepository @Inject constructor(): ConversationRepository {
    companion object {
        private val TAG = FirestoreConversationRepository::class.java.name
    }

    private val firestore = Firebase.firestore
    private val conversationsCollection = firestore.collection(FirestorePath.CONVERSATIONS)

    override suspend fun getByUserIdsSync(userId1: String, userId2: String): Conversation? {
        Log.i(TAG, "userId1: $userId1")
        Log.i(TAG, "userId2: $userId2")

        return try {
            getByUserIdsInternal(userId1, userId2)
        } catch (e: Exception) {
            Log.i(TAG, e.message ?: "")

            try {
                getByUserIdsInternal(userId2, userId1)
            } catch (e: Exception) {
                Log.i(TAG, e.message ?: "")

                null
            }
        }
    }

    suspend fun getByUserIdsInternal(userId1: String, userId2: String): Conversation? {
        val res = conversationsCollection
            .whereEqualTo("userId1", userId1)
            .whereEqualTo("userId2", userId2)
            .get()
            .await()
            .toObjects(Conversation::class.java)

        if (res.size > 0) {
            return res[0]
        }

        throw FirebaseFirestoreException("no results", FirebaseFirestoreException.Code.NOT_FOUND)
    }

    override suspend fun save(conversation: Conversation): Boolean {
        if (conversation.id != null && conversation.id != "") {
            /* user provided id */
            val t = conversation.id?.let { conversationsCollection.document(it).set(conversation) }

            return try {
                t?.await()  // from kotlinx-coroutines-play-services
                true
            } catch (e: java.lang.Exception) {
                false
            }
        } else {
            /* auto-generated id */
            val t = conversationsCollection.add(conversation)

            return try {
                /* updating domain object id */
                conversation.id = t.await().id
                true
            } catch (e: java.lang.Exception) {
                false
            }
        }
    }
}