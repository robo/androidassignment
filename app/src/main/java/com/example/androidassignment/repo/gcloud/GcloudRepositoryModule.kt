package com.example.androidassignment.repo.gcloud

import com.example.androidassignment.repo.GeocodingRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class GcloudRepositoryModule {
    @Binds
    abstract fun bindGeocodingRepository(GcloudGeocodingRepository: GcloudGeocodingRepository): GeocodingRepository
}