package com.example.androidassignment.repo

import androidx.lifecycle.LiveData
import com.example.androidassignment.model.User

interface AuthRepository {
    suspend fun signUp(email: String, password: String): Boolean
    fun isSignedIn(): Boolean
    fun signOut()
    fun getUid(): String
    fun getLogin(): String
    fun getInfo(): LiveData<User>

    suspend fun sendVerificationEmail(): Boolean
}