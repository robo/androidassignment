package com.example.androidassignment.repo

import androidx.lifecycle.LiveData
import com.example.androidassignment.model.Category

interface CategoryRepository {
    fun list(): LiveData<List<Category>>
}