package com.example.androidassignment.repo

import androidx.lifecycle.LiveData
import com.example.androidassignment.model.OrderItem

interface OrderItemRepository {
    fun get(id: String): LiveData<OrderItem>
    fun getByOrder(orderId: String): LiveData<List<OrderItem>>
    suspend fun save(orderItem: OrderItem): Boolean
    suspend fun delete(id: String): Boolean
}