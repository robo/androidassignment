package com.example.androidassignment.repo

import com.google.firebase.firestore.GeoPoint

interface GeocodingRepository {
    fun geocode(address: String): GeoPoint?

    fun reverseGeocode(geoPoint: GeoPoint): String?
}