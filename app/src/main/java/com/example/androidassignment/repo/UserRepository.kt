package com.example.androidassignment.repo

import androidx.lifecycle.LiveData
import com.example.androidassignment.model.User

interface UserRepository {
    fun get(id: String): LiveData<User>
    suspend fun getSync(id: String): User?
    fun getByRole(role: String): LiveData<List<User>>
    suspend fun save(user: User): Boolean
    suspend fun delete(id: String): Boolean
}