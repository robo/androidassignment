package com.example.androidassignment.repo

import androidx.lifecycle.LiveData
import com.example.androidassignment.model.Order

interface OrderRepository {
    fun get(id: String): LiveData<Order>
    fun getByCustomerIdByStatus(customerId: String, status: String): LiveData<List<Order>>
    fun getByOwnerIdByStatus(ownerId: String, status: String): LiveData<List<Order>>
    fun getByRiderIdByStatus(riderId: String, status: String): LiveData<List<Order>>
    fun getByOwnerId(ownerId: String): LiveData<List<Order>>
    suspend fun save(order: Order): Boolean
    suspend fun delete(id: String): Boolean
}