package com.example.androidassignment.repo

import com.example.androidassignment.model.Message

interface MessageRepository {
    suspend fun save(message: Message): Boolean
}