package com.example.androidassignment.repo.firestore

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.livedata.firestore.FirestoreQueryLiveData
import com.example.androidassignment.model.Category
import com.example.androidassignment.repo.CategoryRepository
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import javax.inject.Inject

class FirestoreCategoryRepository @Inject constructor(): CategoryRepository {
    companion object {
//        private val TAG = FirestoreCategoryRepository::class.java.name
    }

    private val firestore = Firebase.firestore
    private val categoriesCollection = firestore.collection(FirestorePath.CATEGORIES)

    override fun list(): LiveData<List<Category>> {
        val collectionLiveData = FirestoreQueryLiveData(categoriesCollection)

        return Transformations.map(collectionLiveData) {
            collectionLiveData.value?.toObjects(Category::class.java)
        }
    }
}