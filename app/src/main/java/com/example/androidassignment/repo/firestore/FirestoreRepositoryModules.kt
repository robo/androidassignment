package com.example.androidassignment.repo.firestore

import com.example.androidassignment.repo.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class UserRepositoryModule {
    @Binds
    abstract fun bindUserRepository(firestoreUserRepository: FirestoreUserRepository): UserRepository
}

@Module
@InstallIn(ViewModelComponent::class)
abstract class ProductRepositoryModule {
    @Binds
    abstract fun bindProductRepository(firestoreProductRepository: FirestoreProductRepository): ProductRepository
}

@Module
@InstallIn(ViewModelComponent::class)
abstract class CategoryRepositoryModule {
    @Binds
    abstract fun bindCategoryRepository(firestoreCategoryRepository: FirestoreCategoryRepository): CategoryRepository
}

@Module
@InstallIn(ViewModelComponent::class)
abstract class OrderRepositoryModule {
    @Binds
    abstract fun bindOrderRepository(firestoreOrderRepository: FirestoreOrderRepository): OrderRepository
}

@Module
@InstallIn(ViewModelComponent::class)
abstract class OrderItemRepositoryModule {
    @Binds
    abstract fun bindOrderItemRepository(firestoreOrderItemRepository: FirestoreOrderItemRepository): OrderItemRepository
}

@Module
@InstallIn(ViewModelComponent::class)
abstract class MessageRepositoryModule {
    @Binds
    abstract fun bindMessageRepository(firestoreMessageRepository: FirestoreMessageRepository): MessageRepository
}

@Module
@InstallIn(ViewModelComponent::class)
abstract class ConversationRepositoryModule {
    @Binds
    abstract fun bindConversationRepository(firestoreConversationRepository: FirestoreConversationRepository): ConversationRepository
}