package com.example.androidassignment.repo

import androidx.lifecycle.LiveData
import com.example.androidassignment.model.Product

interface ProductRepository {
    fun get(id: String): LiveData<Product>
    suspend fun save(product: Product): Boolean
    suspend fun delete(id: String): Boolean
}