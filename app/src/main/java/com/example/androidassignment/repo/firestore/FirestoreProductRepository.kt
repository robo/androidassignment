package com.example.androidassignment.repo.firestore

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.androidassignment.common.FirestorePath
import com.example.androidassignment.livedata.firestore.FirestoreDocumentLiveData
import com.example.androidassignment.model.Product
import com.example.androidassignment.repo.ProductRepository
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import javax.inject.Inject

class FirestoreProductRepository @Inject constructor(): ProductRepository {
    companion object {
        private val TAG = FirestoreProductRepository::class.java.name
    }

    private val firestore = Firebase.firestore
    private val productsCollection = firestore.collection(FirestorePath.PRODUCTS)

    override fun get(id: String): LiveData<Product> {
        val productDocRef = productsCollection.document(id)
        val documentLiveData = FirestoreDocumentLiveData(productDocRef)

        return Transformations.map(documentLiveData) {
            documentLiveData.value?.toObject(Product::class.java)
        }
    }

    override suspend fun save(product: Product): Boolean {
        if (product.id != null && product.id != "") {
            /* user provided id */
            val t = product.id?.let { productsCollection.document(it).set(product) }

            return try {
                t?.await()  // from kotlinx-coroutines-play-services
                true
            } catch (e: Exception) {
                false
            }
        } else {
            /* auto-generated id */
            val t = productsCollection.add(product)

            return try {
                /* updating domain object id */
                product.id = t.await().id
                true
            } catch (e: Exception) {
                false
            }
        }
    }

    override suspend fun delete(id: String): Boolean {
        val t = productsCollection.document(id).delete()

        return try {
            t.await()
            true
        } catch (e: Exception) {
            false
        }
    }
}