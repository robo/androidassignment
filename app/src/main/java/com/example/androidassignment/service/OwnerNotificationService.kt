package com.example.androidassignment.service

import android.app.Service
import android.content.Intent
import android.os.*
import android.os.Process.*
import android.util.Log
import com.example.androidassignment.common.NotificationChannels
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class OwnerNotificationService(): Service() {
    companion object{
        val TAG = OwnerNotificationService::class.java.name
    }

    //the user should be logged in already
    private val auth = FirebaseAuth.getInstance()
    private val firebaseDB = Firebase.firestore
    private val notificationHandler = NotificationHandler()
    private var oldOrderCount: Int = -1

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        firebaseDB
            .collection("orders")
            .whereEqualTo("ownerId", auth.currentUser.uid)
            .whereEqualTo("statusCode", "confirmed")
            .addSnapshotListener { snapShot, error ->
                if(error != null){
                    Log.e(TAG, "Error listening to data: $error")
                    return@addSnapshotListener
                }

                /*when opening application*/
                if(snapShot?.documents?.size == 0){
                    oldOrderCount = 0
                    //do nothing
                }
                else if(snapShot?.documents?.size == oldOrderCount + 1
                    || oldOrderCount == -1 && snapShot?.documents?.size == 1){

                    oldOrderCount = snapShot?.documents?.size
                    notificationHandler
                        .notification(
                            applicationContext,
                            NotificationChannels.CHANNEL_OWNER_RECEIVES_ORDER_ID,
                            android.R.drawable.ic_dialog_info,
                            "You've got a new order!",
                            "Check your orders!"
                        )
                }
                else if(snapShot?.documents?.size!! > oldOrderCount + 1){

                    oldOrderCount = snapShot?.documents?.size
                    notificationHandler
                        .notification(
                            applicationContext,
                            NotificationChannels.CHANNEL_OWNER_RECEIVES_ORDER_ID,
                            android.R.drawable.ic_dialog_info,
                            "${snapShot.documents.size} new orders!",
                            "Check your orders!"
                        )
                }
            }

        // If we get killed, after returning from here, restart
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        // We don't provide binding, so return null
        return null
    }
}