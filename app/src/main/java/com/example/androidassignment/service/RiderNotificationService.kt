package com.example.androidassignment.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.androidassignment.common.NotificationChannels
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class RiderNotificationService: Service() {
    companion object{
        val TAG = RiderNotificationService::class.java.name
    }

    //the user should be logged in already
    private val auth = FirebaseAuth.getInstance()
    private val firebaseDB = Firebase.firestore
    private val notificationHandler = NotificationHandler()
    private var oldOrderCount: Int = -1
    private val channelRiderSelection = NotificationChannels.CHANNEL_RIDER_SELECTION_ID
    private val channelCustomerRiderChat = NotificationChannels.CHANNEL_CUSTOMER_RIDER_CHAT_ID
    private val channelOwnerRiderChat = NotificationChannels.CHANNEL_OWNER_RIDER_ID
    private val icon = android.R.drawable.ic_dialog_info

    /*TODO: notifiche per la chat*/
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand")
        firebaseDB
            .collection("orders")
            .whereEqualTo("riderId", auth.currentUser.uid)
            .whereEqualTo("statusCode", "waiting")
            .addSnapshotListener { snapShot, error ->
                if(error != null){
                    Log.e(TAG, "Error listening to data: $error")
                    return@addSnapshotListener
                }

                /*when opening application*/
                if(snapShot?.documents?.size == 0){
                    oldOrderCount = 0
                    //do nothing
                }
                else if(snapShot?.documents?.size == oldOrderCount + 1
                    || oldOrderCount == -1 && snapShot?.documents?.size == 1){

                    oldOrderCount = snapShot?.documents?.size
                    notificationHandler
                        .notification(
                            applicationContext,
                            channelRiderSelection,
                            icon,
                            "You've been assigned an order!",
                            "Check your orders!"
                        )
                    Log.i(TAG, "snapshot change")
                }
                else if(snapShot?.documents?.size!! > oldOrderCount + 1){

                    oldOrderCount = snapShot?.documents?.size
                    notificationHandler
                        .notification(
                            applicationContext,
                            channelRiderSelection,
                            icon,
                            "You've been assigned ${snapShot.documents.size} new orders!",
                            "Check your orders!"
                        )
                    Log.i(TAG, "snapshot change")
                }
            }

        // If we get killed, after returning from here, restart
        return Service.START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        // We don't provide binding, so return null
        return null
    }
}