package com.example.androidassignment.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.androidassignment.MainActivity
import android.app.PendingIntent as PendingIntent

class NotificationHandler() {
    companion object{
        val TAG = this.javaClass.simpleName
    }
/*
    fun deepLinkNotification(componentName: ComponentName,
                           destination: Int,
                           channelId:String,
                           smallIcon: Int,
                           contentTitle: String,
                           contentText: String
    ){
        val pendingIntent = activity?.let {
            NavDeepLinkBuilder(it.applicationContext)
                .setComponentName(componentName)
                .setGraph(R.navigation.nav_graph)
                .setDestination(destination)
                .createPendingIntent()
        }

        var builder = activity?.let {
            NotificationCompat.Builder(it, channelId)
                .setSmallIcon(smallIcon)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setStyle(
                    NotificationCompat
                        .BigTextStyle()
                        .bigText(contentText)
                )
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
        }

        if (builder != null) {
            with(activity?.let { NotificationManagerCompat.from(it) }){
                this?.notify(1, builder.build())
                Log.i(TAG, "Notification launched!")
            }
        }
    }
*/
    /**
     * usage in the desired fragment:
     *
     *
    context?.let {
        notificationHandler.notification(
            it,
            NotificationChannels.CHANNEL_OWNER_RECEIVES_ORDER_ID,
            android.R.drawable.ic_dialog_info,
            "Titolone",
            "contenutoneeeeeee"
        )
    }
     *
     *
     */
    fun notification(context: Context, channelId: String, icon: Int, title: String, content: String){
        val intent = Intent(context, MainActivity::class.java)
        val pendingIntent: PendingIntent? = TaskStackBuilder.create(context).run {
            addNextIntentWithParentStack(intent)
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        var builder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(icon)
            .setContentTitle(title)
            .setContentText(content)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setStyle(NotificationCompat
                .BigTextStyle()
                .bigText(content)
            )
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)


        if (builder != null) {
            with(NotificationManagerCompat.from(context)) {
                // notificationId is a unique int for each notification that you must define
                this?.notify(1, builder.build())
            }
        }
    }

    fun notificationChannelSetUp(context: Context, channelId: String, channelName: String, description: String){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
                .apply {
                    setDescription(description)
                }
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
            Log.i(TAG, "NotificationManager set")
        }
    }
}