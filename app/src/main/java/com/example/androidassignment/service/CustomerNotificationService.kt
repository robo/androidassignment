package com.example.androidassignment.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.androidassignment.R
import com.example.androidassignment.common.NotificationChannels
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class CustomerNotificationService: Service() {
    companion object{
        val TAG = CustomerNotificationService::class.java.name
    }

    //the user should be logged in already
    private val auth = FirebaseAuth.getInstance()
    private val firebaseDB = Firebase.firestore
    private val notificationHandler = NotificationHandler()
    private var oldOrderCount: Int = -1
    private val channelCustomerRiderChat = NotificationChannels.CHANNEL_CUSTOMER_RIDER_CHAT_ID
    private val icon = android.R.drawable.ic_dialog_info


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand")

        /*when an order is assigned (i.e. the rider starts its delivery)*/
        listenToOrdersByStatusCode("assigned")

        // If we get killed, after returning from here, restart
        return Service.START_STICKY
    }

    private fun listenToOrdersByStatusCode(statusCode: String){
        firebaseDB
            .collection("orders")
            .whereEqualTo("customerId", auth.currentUser.uid)
            .whereEqualTo("statusCode", statusCode)
            .addSnapshotListener { snapShot, error ->
                if(error != null){
                    Log.e(TAG, "Error listening to data: $error")
                    return@addSnapshotListener
                }

                /*when opening application*/
                if(snapShot?.documents?.size == 0){
                    oldOrderCount = 0
                    //do nothing
                }
                else if(snapShot?.documents?.size == oldOrderCount + 1
                    || oldOrderCount == -1 && snapShot?.documents?.size == 1){

                    oldOrderCount = snapShot?.documents?.size
                    notificationHandler
                        .notification(
                            applicationContext,
                            channelCustomerRiderChat,
                            icon,
                            getString(R.string.your_order_has_been_shipped),
                            getString(R.string.you_can_now_chat_with_your_rider)
                        )

                    Log.i(TAG, "snapshot change")
                }
                else if(snapShot?.documents?.size!! > oldOrderCount + 1){

                    oldOrderCount = snapShot?.documents?.size
                    notificationHandler
                        .notification(
                            applicationContext,
                            channelCustomerRiderChat,
                            icon,
                            getString(R.string.your_orders_have_been_shipped),
                            getString(R.string.you_can_now_chat_with_your_riders)
                        )
                    Log.i(TAG, "snapshot change")
                }
            }
    }

    override fun onBind(intent: Intent): IBinder? {
        // We don't provide binding, so return null
        return null
    }
}