package com.example.androidassignment.common

class NotificationChannels {
    companion object{
        const val CHANNEL_CUSTOMER_RIDER_CHAT_ID = "channel_customer_rider_chat_id"
        const val CHANNEL_OWNER_RIDER_ID = "channel_owner_rider_id"
        const val CHANNEL_RIDER_RATINGS_ID = "channel_rider_ratings_id"
        const val CHANNEL_RIDER_SELECTION_ID = "channel_rider_selection_id"
        const val CHANNEL_OWNER_RECEIVES_ORDER_ID = "channel_order_receives_order_id"
    }
}