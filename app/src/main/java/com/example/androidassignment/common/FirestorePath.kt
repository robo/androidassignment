package com.example.androidassignment.common

class FirestorePath {
    companion object {
        const val USERS = "users"
        const val PRODUCTS = "products"
        const val CATEGORIES = "categories"
        const val RATINGS = "ratings"
        const val MESSAGES = "messages"
        const val CONVERSATIONS = "conversations"
        const val ORDERS = "orders"
        const val ORDER_ITEMS = "orderItems"
    }
}