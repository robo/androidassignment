package com.example.androidassignment

import android.Manifest
import android.app.AlertDialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import com.example.androidassignment.common.NotificationChannels
import com.example.androidassignment.service.CustomerNotificationService
import com.example.androidassignment.service.NotificationHandler
import com.example.androidassignment.service.OwnerNotificationService
import com.example.androidassignment.service.RiderNotificationService
import com.example.androidassignment.viewmodel.SharedViewModel
import com.google.android.gms.location.LocationServices
import com.google.firebase.firestore.GeoPoint
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.scheduleAtFixedRate


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    companion object {
        private val TAG = MainActivity::class.java.name
    }

    val model: SharedViewModel by viewModels()
    private val notificationHandler: NotificationHandler = NotificationHandler()
    private var riderTimerTask: TimerTask? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.my_toolbar))

        /* if we are logged in, follow the navigation flow, else return to signin/signup page */
        model.currentUser.observe(this) { currentUser ->
            val v = findViewById<Toolbar>(R.id.my_toolbar)

            if (currentUser?.id != null) {
                /* we are logged in */
                v.title = getString(R.string.current_first_last_name,
                    currentUser.firstName,
                    currentUser.lastName
                )

                val notificationIntent: Intent

                when (currentUser.role) {
                    "owner" -> {
                        notificationHandler.notificationChannelSetUp(this,
                            NotificationChannels.CHANNEL_OWNER_RECEIVES_ORDER_ID,
                            "Owner order notifications",
                            "Notification channel")
                        notificationHandler.notificationChannelSetUp(this,
                            NotificationChannels.CHANNEL_OWNER_RIDER_ID,
                            "Owner-rider chat",
                            "Notification channel")

                        /*Start notification service*/
                        Intent(this, OwnerNotificationService::class.java)
                            .also { intent ->
                                startService(intent)
                            }
                    }
                    "rider" -> {
                        notificationHandler.notificationChannelSetUp(this,
                            NotificationChannels.CHANNEL_CUSTOMER_RIDER_CHAT_ID,
                            "Customer-Rider chat",
                            "Notification channel")
                        notificationHandler.notificationChannelSetUp(this,
                            NotificationChannels.CHANNEL_OWNER_RIDER_ID,
                            "Owner-rider chat",
                            "Notification channel")
                        notificationHandler.notificationChannelSetUp(this,
                            NotificationChannels.CHANNEL_RIDER_SELECTION_ID,
                            "Rider selection",
                            "Notification channel")

                        /*Start notification service*/
                        Intent(this, RiderNotificationService::class.java)
                            .also { intent ->
                                startService(intent)
                            }

                        /* request location service and permissions */
                        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(
                            this)
                        val activity = this

                        if (ActivityCompat.checkSelfPermission(activity,
                                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        ) {
                            Log.i(TAG, "no location perms")

                            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                            ) {
                                AlertDialog.Builder(activity)
                                    .setTitle("Ciao")
                                    .setMessage("Vogliamo tracciarti")
                                    .setPositiveButton("Che devo fa... va bene"
                                    ) { _, _ -> //Prompt the user once explanation has been shown
                                        ActivityCompat.requestPermissions(activity,
                                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                            3456)
                                    }
                                    .create()
                                    .show()
                            } else {
                                ActivityCompat.requestPermissions(activity,
                                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    3456)
                            }
                        }

                        /* schedule recurrent task that updates rider position on the cloud */
                        riderTimerTask = Timer().scheduleAtFixedRate(0, TimeUnit.MINUTES.toMillis(1)) {
                            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                Log.i(TAG, "no location perms")
                            } else fusedLocationClient.lastLocation.addOnSuccessListener {
                                if (it != null) {
                                    val geoPoint = GeoPoint(it.latitude, it.longitude)

                                    Log.i(TAG, "your current location: " + model.reverseGeocode(geoPoint))

                                    currentUser.coordinates = geoPoint
                                    currentUser.address = model.reverseGeocode(geoPoint)

                                    GlobalScope.launch {
                                        if (model.saveCurrentUser(currentUser)) {
                                            Log.i(TAG, "rider position updated")
                                        } else {
                                            Log.i(TAG, "can't update rider location")
                                        }
                                    }

                                } else {
                                    Log.i(TAG, "can't determine your location right now")
                                }
                            }
                        }
                    }
                    "customer" -> {
                        notificationHandler.notificationChannelSetUp(this,
                            NotificationChannels.CHANNEL_RIDER_RATINGS_ID,
                            "Rider ratings",
                            "Notification channel")
                        notificationHandler.notificationChannelSetUp(this,
                            NotificationChannels.CHANNEL_CUSTOMER_RIDER_CHAT_ID,
                            "Customer-Rider chat",
                            "Notification channel")

                        /*Start notification service*/
                        notificationIntent = Intent(this, CustomerNotificationService::class.java)
                        notificationIntent
                            .also { intent ->
                                startService(intent)
                            }
                    }
                }
            } else {
                v.title = getString(R.string.not_signed_in)

                /* cancel rider timer task if was logged in as rider */
                riderTimerTask?.cancel()

                /*dispatch all notification channels*/
                val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                when (currentUser?.role) {      /* TODO(cplrossi): currentUser è null, quindi bisogna trovare altra soluzione */
                    "owner" -> {
                        notificationManager.deleteNotificationChannel(NotificationChannels.CHANNEL_OWNER_RECEIVES_ORDER_ID)
                        notificationManager.deleteNotificationChannel(NotificationChannels.CHANNEL_OWNER_RIDER_ID)
                    }
                    "rider" -> {
                        notificationManager.deleteNotificationChannel(NotificationChannels.CHANNEL_RIDER_SELECTION_ID)
                        notificationManager.deleteNotificationChannel(NotificationChannels.CHANNEL_OWNER_RIDER_ID)
                        notificationManager.deleteNotificationChannel(NotificationChannels.CHANNEL_CUSTOMER_RIDER_CHAT_ID)
                    }
                    "customer" -> {
                        notificationManager.deleteNotificationChannel(NotificationChannels.CHANNEL_CUSTOMER_RIDER_CHAT_ID)
                        notificationManager.deleteNotificationChannel(NotificationChannels.CHANNEL_RIDER_RATINGS_ID)
                    }
                }
            }
        }
    }
}