package com.example.androidassignment.livedata.auth

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.androidassignment.model.User
import com.example.androidassignment.common.FirestorePath
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class AuthLiveData
    : LiveData<User>(), FirebaseAuth.AuthStateListener, EventListener<DocumentSnapshot> {

    companion object {
        private const val TAG = "AuthLiveData"
    }

    /* Firebase cloud dependencies */
    private val auth = Firebase.auth
    private val firestore = Firebase.firestore

    private val usersCollection = firestore.collection(FirestorePath.USERS)
    private val dummyUser by lazy { User() }
    private var listener: ListenerRegistration? = null  // firestore listener subscription
    private var isSignedIn = auth.currentUser != null

    override fun onActive() {
        Log.d(TAG, "adding authstate listener")

        auth.addAuthStateListener(this)
    }

    override fun onInactive() {
        Log.i(TAG, "removing authstate listener")

        auth.removeAuthStateListener(this)
    }

    override fun onAuthStateChanged(a: FirebaseAuth) {
        isSignedIn = a.currentUser != null

        if (isSignedIn) {
            Log.d(TAG, "we are signed in")

            val userDocumentRef = auth.currentUser?.uid?.let {
                usersCollection.document(it)
            }

            listener = userDocumentRef?.addSnapshotListener(this)
        } else {
            Log.d(TAG, "we are signed out")

            listener?.remove()

            postValue(dummyUser)
        }
    }

    override fun onEvent(snapshot: DocumentSnapshot?, e: FirebaseFirestoreException?) {
        /* current user should be NEVER null */
        if (e == null && snapshot != null) {
            Log.i(TAG, "received new current user snapshot")

            val user = snapshot.toObject(User::class.java)

            if (user != null) {
                postValue(user)
            } else {
                postValue(dummyUser)
            }
        } else {
            if (e != null ) {
                Log.i(TAG, "exception: " + e.message)
            }

            postValue(dummyUser)
        }
    }
}