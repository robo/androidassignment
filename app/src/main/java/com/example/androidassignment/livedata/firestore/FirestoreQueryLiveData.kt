package com.example.androidassignment.livedata.firestore

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*

class FirestoreQueryLiveData(private val query: Query)
    : LiveData<QuerySnapshot>(), EventListener<QuerySnapshot> {

    companion object {
        private const val TAG = "FirestoreQueryLiveData"
    }

    lateinit var listener: ListenerRegistration

    override fun onActive() {
        Log.i(TAG, "adding firestore query listener")

        listener = query.addSnapshotListener(this)
    }

    override fun onInactive() {
        Log.i(TAG, "removing firestore query listener")

        listener.remove()
    }

    override fun onEvent(snapshot: QuerySnapshot?, e: FirebaseFirestoreException?) {
        if (snapshot != null) {
            postValue(snapshot)
        }

        /* TODO(cplrossi): handle exception */
    }
}