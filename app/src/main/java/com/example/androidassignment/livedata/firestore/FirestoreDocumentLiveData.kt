package com.example.androidassignment.livedata.firestore

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*

class FirestoreDocumentLiveData(private val ref: DocumentReference)
    : LiveData<DocumentSnapshot>(), EventListener<DocumentSnapshot> {
    companion object {
        private const val TAG = "FirestoreDocumentLiveData"
    }

    lateinit var listener: ListenerRegistration

    override fun onActive() {
        Log.i(TAG, "adding firestore document listener")

        listener = ref.addSnapshotListener(this)
    }

    override fun onInactive() {
        Log.i(TAG, "removing firestore document listener")

        listener.remove()
    }

    override fun onEvent(snapshot: DocumentSnapshot?, e: FirebaseFirestoreException?) {
        if (snapshot != null) {
            Log.i(TAG, "new firestore snapshot")

            postValue(snapshot)
        }

        /* TODO(cplrossi): handle exception */
    }
}