package com.example.androidassignment

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.androidassignment.model.Product
import com.example.androidassignment.repo.ProductRepository
import com.example.androidassignment.repo.firestore.FirestoreProductRepository
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ProductRepositoryTest {
    /*************************************/
    private val email = "owner@mail.com"
    private val password = "testPassword"
    private val listSize = 10
    /*************************************/

    private val auth = Firebase.auth
    private val productRepository: ProductRepository by lazy { FirestoreProductRepository() }
    private lateinit var products: List<Product>

    @Before
    fun initTest() {
        Tasks.await(auth.signInWithEmailAndPassword(email, password))

        products = List(listSize) {
            Product(null,   // self-generated id
                auth.currentUser?.uid,
                "1",
                ('a' + it).toString(),
                .0F,
                ('z' - it).toString(),
                ""
            )
        }
    }

    @Test
    fun testGet() {
        assertTrue(true)
    }

    @Test
    fun testSave() {
        for (product in products) {
            var res: Boolean

            runBlocking {
                res = productRepository.save(product)
            }

            println(product.id)

            assertTrue(res)

            runBlocking {
                product.id?.let {
                    res = productRepository.delete(it)
                }
            }

            assertTrue(res)
        }
    }

    @Test
    fun testDelete() {
        assertTrue(true)
    }
}
