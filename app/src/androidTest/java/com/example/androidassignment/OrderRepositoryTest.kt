package com.example.androidassignment

import com.example.androidassignment.model.Order
import com.example.androidassignment.repo.OrderRepository
import com.example.androidassignment.repo.firestore.FirestoreOrderRepository
import com.google.android.gms.tasks.Tasks
import com.google.firebase.Timestamp
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import org.junit.Before
import java.util.*

class OrderRepositoryTest {
    /*************************************/
    private val email = "test@mail.com"     // that's a customer
    private val password = "testPassword"
    private val listSize = 10
    /*************************************/

    private val auth = Firebase.auth
    private val orderRepository: OrderRepository by lazy { FirestoreOrderRepository() }
    private lateinit var orders: List<Order>

    @Before
    fun initTest() {
        Tasks.await(auth.signInWithEmailAndPassword(email, password))

         orders = List(listSize) {
            Order(
                null,   // self-generated id
                auth.currentUser?.uid,
                "1",
                null,
                "pending",
                Timestamp(Date(System.currentTimeMillis())),
                null,
                ('a' + it).toString(),
                it.toFloat()
            )
        }
    }
}