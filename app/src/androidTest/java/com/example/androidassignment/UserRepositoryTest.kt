package com.example.androidassignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.androidassignment.model.User
import com.example.androidassignment.repo.UserRepository
import com.example.androidassignment.repo.firestore.FirestoreUserRepository
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserRepositoryTest {
    /*************************************/
    private val email = "owner@mail.com"
    private val password = "testPassword"
    /*************************************/

    private val auth = Firebase.auth
    private val userRepository: UserRepository by lazy { FirestoreUserRepository() }

    @get:Rule
    var activityScenarioRule = activityScenarioRule<MainActivity>()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun initTest() {
        Tasks.await(auth.signInWithEmailAndPassword(email, password))
    }

    @Test
    fun testGet() {
        var user: LiveData<User>

        runBlocking {
            user = userRepository.get(auth.currentUser!!.uid)
        }

        user.observeForever {
            println(user.value?.firstName + " " + user.value?.lastName)
        }
    }
}