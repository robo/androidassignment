package com.example.androidassignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.test.ext.junit.rules.activityScenarioRule
import com.example.androidassignment.model.Category
import com.example.androidassignment.repo.CategoryRepository
import com.example.androidassignment.repo.firestore.FirestoreCategoryRepository
import com.google.android.gms.tasks.Tasks
import com.google.common.util.concurrent.Service
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CategoryRepositoryTest {
    /*************************************/
    private val email = "owner@mail.com"
    private val password = "testPassword"
    /*************************************/

    private val auth = Firebase.auth
    private val categoryRepository: CategoryRepository by lazy { FirestoreCategoryRepository() }

    @get:Rule
    var activityScenarioRule = activityScenarioRule<MainActivity>()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun initTest() {
        Tasks.await(auth.signInWithEmailAndPassword(email, password))
    }

    @Test
    fun testGet() {
        var categories: LiveData<List<Category>>

        runBlocking {
            categories = categoryRepository.list()
        }

        categories.observeForever {
            for (category in categories.value!!) {
                println(category.name)
            }
        }
    }
}